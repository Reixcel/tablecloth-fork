#include "render.hpp"

#include "util/logging.hpp"

#include "output.hpp"
#include "seat.hpp"
#include "server.hpp"
#include "view.hpp"
#include "render_utils.hpp"

namespace cloth::render {

// Context already contains a vector of RenderData.

Context::Context(Output& output)
    : output(output),
      damage(wlr_output_damage_create(&output.wlr_output))
{
    renderer = wlr_backend_get_renderer(output.wlr_output.backend);
    assert(renderer);
}

//////////////////////////////////////////
// Context render functions
//////////////////////////////////////////

// Alert all layer_shell surfaces when frame is done rendering
void Context::layers_send_done()
{
    auto when_ts = chrono::to_timespec(when);
    for (auto& layer : output.layers) {
        for (auto& surface : layer) {
            // Can't find layer_surface member
            wlr::layer_surface_v1_t& layer = surface.layer_surface;
            wlr_surface_send_frame_done(layer.surface, &when_ts);
            wlr::xdg_popup_t* popup;
            wl_list_for_each(popup, &surface.layer_surface.popups, link)
            {
                wlr_surface_send_frame_done(popup->base->surface, &when_ts);
            }
        }
    }
}

// drawing will only be possible within the box given by rect 
void scissor_output(Output& output,
                           pixman_box32_t* rect)
{
    wlr::renderer_t* renderer = wlr_backend_get_renderer(output.wlr_output.backend);
    assert(renderer);

    wlr::box_t obox = {
        .x = rect->x1,
        .y = rect->y1,
        .width = rect->x2 - rect->x1,
        .height = rect->y2 - rect->y1,
    };

    int ow, oh;
    wlr_output_transformed_resolution(&output.wlr_output, &ow, &oh);

    auto transform = wlr_output_transform_invert(output.wlr_output.transform);
    wlr_box_transform(&obox, transform, ow, oh, &obox);

    wlr_renderer_scissor(renderer, &obox);
}

// Callback. Render any surface
void Context::render_surface(wlr::surface_t* surface,
                             int sx,
                             int sy,
                             void* _data)
{
    if (!surface) {
        // cloth_error("null surface in render_surface");
        return;
    }

    auto& data = *(SurfaceRenderData*)_data;
    Output& output = data.context.output;

    wlr::renderer_t* renderer = wlr_backend_get_renderer(output.wlr_output.backend);
    assert(renderer);

    wlr::texture_t* texture = wlr_surface_get_texture(surface);
    if (texture == nullptr) {
        return;
    }

    wlr::box_t box = { 
        .x = int(data.parent_data.lx + sx),
        .y = int(data.parent_data.ly + sy),
        .width = int(surface->current.width * data.x_scale),
        .height = int(surface->current.height * data.y_scale) 
    };

    pixman_region32_t damage;
    pixman_region32_init(&damage);
    pixman_region32_union_rect(
        &damage, &damage, box.x, box.y, box.width, box.height);
    pixman_region32_intersect(&damage, &damage, &data.context.pixman_damage);
    bool damaged = pixman_region32_not_empty(&damage);
    if (damaged) {
        float matrix[9];
        auto transform = wlr_output_transform_invert(surface->current.transform);
        wlr_matrix_project_box(matrix,
                               &box,
                               transform,
                               0,
                               output.wlr_output.transform_matrix);

        int nrects;
        pixman_box32_t* rects = pixman_region32_rectangles(&damage, &nrects);
        for (int i = 0; i < nrects; ++i) {
            scissor_output(output, &rects[i]);
            wlr_render_texture_with_matrix(renderer,
                                           texture,
                                           matrix,
                                           data.parent_data.alpha);
        }
    }

    pixman_region32_fini(&damage);
};

// Tell the surface the frame is rendered and that it should prepare the next?
static void surface_send_frame_done(wlr::surface_t* surface,
                                    int sx,
                                    int sy,
                                    void* _data)
{
    auto& cvd = *(SurfaceRenderData*)_data;
    auto when = chrono::to_timespec(cvd.context.when);

    double lx, ly;
    lx = cvd.parent_data.lx + sx;
    ly = cvd.parent_data.ly + sy;

    // Test for popups being outside the output
    // Toplevels should always intersect
    if (!surface_intersect_output(*surface,
                                  *cvd.context.output.desktop.layout,
                                  cvd.context.output.wlr_output,
                                  lx,
                                  ly,
                                  nullptr))
    {
        return;
    }

    wlr_surface_send_frame_done(surface, &when);
}

// Renders a view with decorations and all subsurfaces
void Context::render(View& view, ViewRenderData& data)
{
    // Do not render views fullscreen on other outputs
    if (view.fullscreen_output != nullptr && view.fullscreen_output != &output) {
        return;
    }

    render_decoration(view, data);
    for_each_surface(view, render_surface, data);
}

// Renders all layer_shell surfaces in a layer
void Context::render(util::ptr_vec<LayerSurface>& layer)
{
    for (auto& layer_surface : layer) {
        // TODO: alpha, rotation and scaling for layer surfaces.
        ViewRenderData data = {
            .lx = layer_surface.geo.x + (double)output_box->x,
            .ly = layer_surface.geo.y + (double)output_box->y,
            .width = (double)layer_surface.layer_surface.surface->current.width,
            .height = (double)layer_surface.layer_surface.surface->current.height
        };
        for_each_surface(*layer_surface.layer_surface.surface, render_surface, data);

        if (layer_surface.has_shadow) {
            draw_shadow(data, 0.4f, layer_surface.shadow_radius, layer_surface.shadow_offset);
        }

        SurfaceRenderData surfdat = { *this, data };
        wlr_layer_surface_v1_for_each_surface(
            &layer_surface.layer_surface, render_surface, &surfdat);
    }
}

// Reset the Context to default state?
void Context::reset()
{
    views.clear();
    fullscreen_view = nullptr;
    clear_color = { 0.25f, 0.25f, 0.25f, 1.0f };
}

// Calls render for the view(s) that needs to be rendered
// TODO: This function is too long, split it up.
void Context::do_render()
{
    when = chrono::clock::now();

    // TODO: Can also be fetched when constructing?
    output_box = wlr_output_layout_get_box(output.desktop.layout, &output.wlr_output);

    // Check if we can delegate the fullscreen surface to the output
    if (false && fullscreen_view && fullscreen_view->wlr_surface != nullptr) {
        View& view = *fullscreen_view;

        // Make sure the view is centered on screen
        // TODO: Isn't the view already maximized?
        // The coords given by get_box should be correct
        wlr::box_t view_box = view.get_box();
        double view_x = (double)(output_box->width - view_box.width) / 2 + output_box->x;
        double view_y = (double)(output_box->height - view_box.height) / 2 + output_box->y;
        view.move(view_x, view_y);

        if (view.is_standalone()) {
            wlr_output_set_fullscreen_surface(&output.wlr_output, view.wlr_surface);
        } else {
            wlr_output_set_fullscreen_surface(&output.wlr_output, nullptr);
        }

        // Fullscreen views are rendered on a black background
        // TODO: Should probably be rendered over the wallpaper,
        // e.g for see-thorugh terminals
        clear_color = { 0.f, 0.f, 0.f, 1.f };
    } else {
        wlr_output_set_fullscreen_surface(&output.wlr_output, nullptr);
    }

    bool needs_swap;
    pixman_region32_init(&pixman_damage);
    if (!wlr_output_damage_make_current(this->damage, &needs_swap, &pixman_damage)) {
        return;
    }

    // otherwise Output doesn't need swap and isn't damaged,
    // skip rendering completely
    if (needs_swap) {
        wlr_renderer_begin(renderer, output.wlr_output.width, output.wlr_output.height);

        // otherwise Output isn't damaged but needs buffer swap
        if (pixman_region32_not_empty(&pixman_damage)) {
            if (output.desktop.server.config.debug_damage_tracking) {
                float color[] = { 1, 1, 0, 1 };
                wlr_renderer_clear(renderer, color);
            }

            // TODO: I think this is the background color, I wonder
            // if this should be removed if there is a background image.
            // Does it cost a lot?
            int nrects;
            pixman_box32_t* rects = pixman_region32_rectangles(&pixman_damage, &nrects);
            for (int i = 0; i < nrects; ++i) {
                scissor_output(output, &rects[i]);
                wlr_renderer_clear(renderer, clear_color.data());
            }

            render(output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND]);
            render(output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM]);

            // If a view is fullscreen on the output, render it
            if (fullscreen_view) {
                auto& view = *fullscreen_view;
                ViewRenderData data = { 
                    .lx = 0,
                    .ly = 0,
                    .width = (double)output.wlr_output.width,
                    .height = (double)output.wlr_output.height,
                    .alpha = 1.f 
                };

                if (output.wlr_output.fullscreen_surface == view.wlr_surface) {
                    // The output will render the fullscreen view
                } else {
                    if (view.wlr_surface != nullptr) {
                        for_each_surface(view, render_surface, data);
                    }

                    // During normal rendering the xwayland window tree isn't
                    // traversed because all windows are rendered. Here we only want
                    // to render the fullscreen window's children so we have to
                    // traverse the tree.
#ifdef WLR_HAS_XWAYLAND
                    if (auto* xwayland_surface = dynamic_cast<XwaylandSurface*>(&view);
                        xwayland_surface) {
                        for_each_surface(
                            *xwayland_surface->xwayland_surface, render_surface, data);
                    }
                }
#endif
            } else {
                // Render all views
                for (auto& vd : views) {
                    render(vd.view, vd.data);
                }
            }

            // Render top layer above shell views
            render(output.layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP]);

            // Render drag icons
            ViewRenderData data{ .alpha = 1.f };
            for_each_drag_icon(output.desktop.server.input, render_surface, data);

            render(output.layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY]);
        }

        wlr_renderer_scissor(renderer, nullptr);
        wlr_renderer_end(renderer);

        // TODO: What? Set the pixman_damage area to the output region?
        if (output.desktop.server.config.debug_damage_tracking) {
            int width, height;
            wlr_output_transformed_resolution(&output.wlr_output,
                                              &width,
                                              &height);
            pixman_region32_union_rect(&pixman_damage,
                                       &pixman_damage,
                                       0, 0, width, height);
        }

        // PREV: update now?
        struct timespec now_ts = chrono::to_timespec(when);
        if (wlr_output_damage_swap_buffers(
                    this->damage, &now_ts, &pixman_damage)) {
            when = chrono::to_time_point(now_ts);
            output.last_frame = output.desktop.last_frame = when;
        }
    }

    damage_done();
}

// Send frame_done to all views and layer_shells
void Context::damage_done()
{
    // Damage finish
    pixman_region32_fini(&pixman_damage);

    // Send frame done events to all surfaces
    if (fullscreen_view) {
        auto& view = *fullscreen_view;
        if (output.wlr_output.fullscreen_surface == view.wlr_surface) {
            // The surface is managed by the output.wlr_output
            return;
        }

        ViewRenderData data = {
            .lx = 0,
            .ly = 0,
            .width = (double)output.wlr_output.width,
            .height = (double)output.wlr_output.height,
            .alpha = 1.f
        };

        for_each_surface(view, surface_send_frame_done, data);

#ifdef WLR_HAS_XWAYLAND
        if (auto* xwayland_surface = dynamic_cast<XwaylandSurface*>(&view); xwayland_surface) {
            for_each_surface(*xwayland_surface->xwayland_surface,
                             surface_send_frame_done,
                             data);
        }
#endif
    } else {
        for (auto& [view, data] : views) {
            for_each_surface(view, surface_send_frame_done, data);
        }

        ViewRenderData data{ .alpha = 1.f };
        for_each_drag_icon(output.desktop.server.input,
                           surface_send_frame_done,
                           data);
    }
    layers_send_done();
}

//////////////////////////////////////////
// Context damage functions
//////////////////////////////////////////

// If the view is visible is accepts damages
static bool view_accept_damage(Output& output, View& view)
{
    if (view.wlr_surface == nullptr) {
        return false;
    }
    if (output.workspace->fullscreen_view == nullptr) {
        return true;
    }
    if (output.workspace->fullscreen_view == &view) {
        return true;
    }
#ifdef WLR_HAS_XWAYLAND
    {
        auto* xfv = dynamic_cast<XwaylandSurface*>(output.workspace->fullscreen_view);
        auto* xv = dynamic_cast<XwaylandSurface*>(&view);
        if (xfv && xv) {
            // Special case: accept damage from children
            auto* xsurface = xv->xwayland_surface;
            while (xsurface != nullptr) {
                if (xfv->xwayland_surface == xsurface) {
                    return true;
                }
                xsurface = xsurface->parent;
            }
        }
    }
#endif
    return false;
}

// Damage the entire output
void Context::damage_whole()
{
    wlr_output_damage_add_whole(damage);
}

// Add damage of any surface
// TODO: This function is called many places without providing scale
// maybe BUG
static void damage_whole_surface(wlr::surface_t* surface, int sx, int sy, void* _data)
{
    auto& [context, data, x_scale, y_scale] = *(SurfaceRenderData*)_data;

    double lx = data.lx + sx * x_scale;
    double ly = data.ly + sy * y_scale;

    // Surface has to have buffer
    if (!wlr_surface_has_buffer(surface)) {
        return;
    }

    // Get output width and hight after output flip
    // NOT USED?
    int ow, oh;
    wlr_output_transformed_resolution(&context.output.wlr_output, &ow, &oh);

    wlr::box_t box;
    bool intersects = surface_intersect_output(*surface,
                                               *context.output.desktop.layout,
                                               context.output.wlr_output,
                                               lx,
                                               ly,
                                               &box);
    if (!intersects) {
        return;
    }

    wlr_output_damage_add_box(context.damage, &box);
}

void Context::damage_whole_local_surface(wlr::surface_t& surface,
                                         double ox,
                                         double oy)
{
    wlr::output_layout_output_t* layout =
        wlr_output_layout_get(output.desktop.layout, &output.wlr_output);

    ViewRenderData data = { 
        .lx = ox + layout->x,
        .ly = oy + layout->y,
    };

    for_each_surface(surface, damage_whole_surface, data);
}

void Context::damage_whole_layer(LayerSurface& layer, wlr::box_t geo) 
{
    wlr::output_layout_output_t* layout = 
        wlr_output_layout_get(output.desktop.layout, &output.wlr_output);

    ViewRenderData data = {.lx = (double) geo.x + layout->x,
                           .ly = (double) geo.y + layout->y};

    for_each_surface(*layer.layer_surface.surface, damage_whole_surface, data);

    if (layer.has_shadow) {
        wlr::box_t shadow_box = geo;
        shadow_box.x += layer.shadow_offset - layer.shadow_radius / 2.f + layout->x;
        shadow_box.y += layer.shadow_offset - layer.shadow_radius / 2.f + layout->y;
        shadow_box.width += layer.shadow_radius;
        shadow_box.height += layer.shadow_radius;
        wlr_output_damage_add_box(damage, &shadow_box);
    }
}

// Add damage for an entire view
void Context::damage_whole_view(View& view)
{
    if (!view_accept_damage(output, view)) {
        return;
    }

    damage_whole_decoration(view);

    ViewRenderData data{
        .lx = view.lx,
        .ly = view.ly,
        .width = (double) view.width,
        .height = (double) view.height
    };

    for_each_surface(view, damage_whole_surface, data);
}

// Add damage for the full icon being dragged
void Context::damage_whole_drag_icon(DragIcon& icon)
{
    ViewRenderData data{
        .lx = icon.x, 
        .ly = icon.y,
    };

    for_each_surface(*icon.wlr_drag_icon.surface, damage_whole_surface, data);
}

// Damage part of a surface
// Don't really understand how this is done
static void damage_from_surface(wlr::surface_t* surface, int sx, int sy, void* _data)
{
    auto& [context, data, x_scale, y_scale] = *(SurfaceRenderData*)_data;
    wlr::output_t& wlr_output = context.output.wlr_output;

    double lx = data.lx + sx * x_scale;
    double ly = data.ly + sy * y_scale;

    // TODO: Why so late
    if (!wlr_surface_has_buffer(surface)) {
        return;
    }

    // If output is flipped the height and width is swapped
    // TODO: This isn't used?
    int ow, oh;
    wlr_output_transformed_resolution(&wlr_output, &ow, &oh);

    wlr::box_t box;
    surface_intersect_output(
        *surface, *context.output.desktop.layout, wlr_output, lx, ly, &box);

    enum wl_output_transform transform =
        wlr_output_transform_invert(surface->current.transform);

    // Transfer buffer_damage, flip and stretch it
    pixman_region32_t damage;
    pixman_region32_init(&damage);
    pixman_region32_copy(&damage, &surface->buffer_damage);
    wlr_region_transform(&damage,
                         &damage,
                         transform,
                         surface->current.buffer_width,
                         surface->current.buffer_height);
    // Scale the damaged region depending on the scaling of the output
    // relative to the scaling of the surface. 
    wlr_region_scale(&damage, &damage, wlr_output.scale / (float)surface->current.scale);
    // Make the rectangles a distance longer in every direction
    // Seems kinda arbitrary to me, I don't understand
    if (std::ceil(wlr_output.scale) > surface->current.scale) {
        // When scaling up a surface, it'll become blurry so we need to
        // expand the damage region
        wlr_region_expand(
            &damage, &damage, std::ceil(wlr_output.scale) - surface->current.scale);
    }
    pixman_region32_translate(&damage, box.x, box.y);
    wlr_output_damage_add(context.damage, &damage);
    pixman_region32_fini(&damage);
}

// Add damage from every surface and subsurface
void Context::damage_from_local_surface(wlr::surface_t& surface,
                                              double ox,
                                              double oy)
{
    wlr::output_layout_output_t* layout =
        wlr_output_layout_get(output.desktop.layout, &output.wlr_output);
    ViewRenderData data = {
        .lx = ox + layout->x,
        .ly = oy + layout->y};
    for_each_surface(surface, damage_from_surface, data);
}

// Add damage from every surface in view
auto Context::damage_from_view(View& view) -> void
{
    if (!view_accept_damage(output, view)) {
        return;
    }

    ViewRenderData data = {
        .lx = view.lx,
        .ly = view.ly,
        .width = (double) view.width,
        .height = (double) view.height
    };

    for_each_surface(view, damage_from_surface, data);
}

//////////////////////////////////////////
// Context for_each wrappers
//////////////////////////////////////////

// Calls iterator for root surface and all subsurfaces
void Context::for_each_surface(wlr::surface_t& surface,
                               wlr_surface_iterator_func_t iterator,
                               const ViewRenderData& data)
{
    SurfaceRenderData rd = {
        .context = *this,
        .parent_data = data,
        .x_scale = data.width / double(surface.current.width),
        .y_scale = data.height / double(surface.current.height)
    };

    wlr_surface_for_each_surface(&surface, iterator, &rd);
}

// Calls iterator for root surface of view and all subsurfaces
void Context::for_each_surface(View& view,
                               wlr_surface_iterator_func_t iterator,
                               const ViewRenderData& data)
{
    if (!view.wlr_surface) {
        // cloth_error("Null surface for view title='{}', type='{}'", view.get_name(),
        // (int) view.type());
        return;
    }

    SurfaceRenderData rd = {
        .context = *this,
        .parent_data = data,
        .x_scale = data.width / double(view.width),
        .y_scale = data.height / double(view.height)
    };

    if (auto* xdg_surface_v6 = dynamic_cast<XdgSurfaceV6*>(&view); xdg_surface_v6) {
        wlr_xdg_surface_v6_for_each_surface(xdg_surface_v6->xdg_surface, iterator, &rd);
    } else if (auto* xdg_surface = dynamic_cast<XdgSurface*>(&view); xdg_surface) {
        wlr_xdg_surface_for_each_surface(xdg_surface->xdg_surface, iterator, &rd);
    } else if (auto* wl_shell_surface = dynamic_cast<WlShellSurface*>(&view); wl_shell_surface) {
        wlr_wl_shell_surface_for_each_surface(wl_shell_surface->wl_shell_surface,
                                              iterator,
                                              &rd);
#ifdef WLR_HAS_XWAYLAND
    } else if (auto* wlr_surface = dynamic_cast<XwaylandSurface*>(&view); wlr_surface) {
        wlr_surface_for_each_surface(wlr_surface->xwayland_surface->surface,
                                     iterator, 
                                     &rd);
#endif
    }
}

#ifdef WLR_HAS_XWAYLAND
// overload for xwayland surfaces
void Context::for_each_surface(wlr::xwayland_surface_t& surface,
                               wlr_surface_iterator_func_t iterator,
                               ViewRenderData data)
{
    wlr::xwayland_surface_t* child;
    wl_list_for_each(child, &surface.children, parent_link)
    {
        if (child == nullptr || child->surface == nullptr) continue;
        // TODO: make relative to parent size and position
        data.lx = child->x;
        data.ly = child->y;
        data.width = child->surface->current.width;
        data.height = child->surface->current.height;
        if (child->mapped) {
            for_each_surface(*child->surface, iterator, data);
        }
        for_each_surface(*child, iterator, data);
    }
}
#endif

// Call iterator on all icons that are being dragged
void Context::for_each_drag_icon(Input& input,
                                       wlr_surface_iterator_func_t iterator,
                                       ViewRenderData data)
{
    for (auto& seat : input.seats) {
        for (auto& drag_icon : seat.drag_icons) {
            if (drag_icon.wlr_drag_icon.mapped) {
                data.lx = drag_icon.x;
                data.ly = drag_icon.y;
                data.width = drag_icon.wlr_drag_icon.surface->current.width;
                data.height = drag_icon.wlr_drag_icon.surface->current.height;

                for_each_surface(*drag_icon.wlr_drag_icon.surface, iterator, data);
            }
        }
    }
}

} // namespace cloth
