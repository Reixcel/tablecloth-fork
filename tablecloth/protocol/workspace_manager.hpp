#pragma once

#include <wayland-server.h>

#include "wlroots.hpp"

namespace cloth {

struct Server;

//
struct WorkspaceManager {
    void switch_to(int idx);
    void move_surface(wl::resource_t*, int ws_idx);

    void send_state();

    WorkspaceManager(Server&);
    ~WorkspaceManager() noexcept;

    Server& server;
    wl::global_t* global;
    std::vector<wl::resource_t*> bound_clients;
};

}  // namespace cloth
