#pragma once

#include <wayland-server.h>

#include "wlroots.hpp"

namespace cloth {

struct Server;
struct Workspace;

struct WindowManager {
    void cycle_focus();
    void run_command(const char*);

    void send_focused_window_name(Workspace& ws);

    WindowManager(Server&);
    ~WindowManager() noexcept;

    Server& server;
    wl::global_t* global;
    std::vector<wl::resource_t*> bound_clients;
};

}  // namespace cloth
