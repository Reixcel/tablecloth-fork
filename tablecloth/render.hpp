#pragma once

#include <pixman.h>

#include "util/chrono.hpp"
#include "util/macros.hpp"
#include "util/ptr_vec.hpp"

#include "layer_shell.hpp"
#include "wlroots.hpp"

#include "wlr-layer-shell-unstable-v1-protocol.h"

namespace cloth {

    struct View;
    struct DragIcon;
    struct Output;
    struct Input;

    namespace render {

    struct Context;

    // A View's data may be manipulated before being drawn i.e rotation.
    struct ViewRenderData {
        double lx = 0;
        double ly = 0;
        double width = 0;
        double height = 0;
        float alpha = 1;

        operator wlr::box_t() const noexcept
        {
            return {(int) lx, (int) ly, (int) width, (int) height};
        }

        bool operator==(const ViewRenderData& rhs) {
            return memcmp(this, &rhs, sizeof(ViewRenderData));
            //return (lx == rhs.lx) && (ly == rhs.ly) && (width == rhs.width) &&
            //    (height == rhs.height) && (alpha == rhs.alpha);
        };

        bool operator!=(const ViewRenderData& rhs) { 
            return !(*this == rhs);
        };
    };

    // For static callback functions that need access to an instance
    // TODO: Maybe the function can be bound to the instance instead
    // of having to have a separate struct
    struct SurfaceRenderData {
        Context& context;
        // The data for the toplevel view this surface is linked to.
        ViewRenderData parent_data;
        // The scaling applied.
        double x_scale = 1.0;
        double y_scale = 1.0;
    };

    // A view and its modified render attributes
    struct ViewAndData {
        constexpr ViewAndData(View& view, ViewRenderData data = {}) noexcept
            : view(view), data(data){};
        View& view;
        ViewRenderData data;
    };

    // One Context per output
    struct Context {
        Context(Output& output);

        void do_render();

        void damage_whole();
        void damage_whole_layer(LayerSurface& layer_surface, wlr::box_t geo);
        void damage_whole_layer(LayerSurface& layer) { damage_whole_layer(layer, layer.geo); };
        void damage_whole_view(View& view);
        void damage_whole_drag_icon(DragIcon& icon);
        void damage_whole_local_surface(wlr::surface_t& surface,
                                        double ox,
                                        double oy);
        void damage_whole_decoration(View& view);
        void damage_from_view(View& view);
        void damage_from_local_surface(wlr::surface_t& surface,
                                       double ox,
                                       double oy);
        void reset();

        // DATA //
        Output& output;

        wlr::renderer_t* renderer = nullptr;
        chrono::time_point when = chrono::clock::now();
        std::vector<ViewAndData> views;
        std::array<float, 4> clear_color = {0.25f, 0.25f, 0.25f, 1.0f};
        View* fullscreen_view = nullptr;
        wlr::output_damage_t* damage;
        wlr::box_t* output_box;

      private:
        void render(View&, ViewRenderData&);
        void render(util::ptr_vec<LayerSurface>&);
        void draw_shadow(wlr::box_t box, float alpha, float radius, float offset);
        void render_decoration(View&, ViewRenderData&);

        void damage_done();
        void layers_send_done();

        void for_each_surface(wlr::surface_t& surface,
                              wlr_surface_iterator_func_t iterator,
                              const ViewRenderData& data);

        void for_each_surface(View&,
                              wlr_surface_iterator_func_t,
                              const ViewRenderData&);

        void for_each_drag_icon(Input& input,
                                wlr_surface_iterator_func_t iterator,
                                ViewRenderData data);

#ifdef WLR_HAS_XWAYLAND
        void for_each_surface(wlr::xwayland_surface_t& surface,
                              wlr_surface_iterator_func_t,
                              ViewRenderData);
#endif

        /// Callback for wlr for_each functions
        ///
        /// parameter data is of type SurfaceRenderData
        static void render_surface(wlr::surface_t* surface, 
                                   int sx, 
                                   int sy,
                                   void* data);

        pixman_region32 pixman_damage;
    };

    } // namespace render
} // namespace cloth
