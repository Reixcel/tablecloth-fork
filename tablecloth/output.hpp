#pragma once

#include "util/chrono.hpp"
#include "util/ptr_vec.hpp"

#include "layer_shell.hpp"
#include "render.hpp"
#include "wlroots.hpp"

namespace cloth {

struct Workspace;
struct Desktop;

// Keep track of a connected output.
// Displays the workspace of a desktop and can damage different parts.
struct Output
{
    Output(Desktop& desktop, Workspace& ws, wlr::output_t& wlr) noexcept;

    Output(const Output&) = delete;
    Output& operator=(const Output&) noexcept = delete;

    Output(Output&& rhs) noexcept = default;
    Output& operator=(Output&&) = default;

    // Member variables
    Desktop& desktop;

    std::array<util::ptr_vec<LayerSurface>, 4> layers;

    util::non_null_ptr<Workspace> workspace;
    wlr::output_t& wlr_output;

    chrono::time_point last_frame;
    
    // The box after exclusive zones for layer shells are set
    // in output local coordinates
    wlr::box_t usable_area; 

    render::Context context = {*this};

  protected:
    wl::Listener on_destroy;        // output removed
    wl::Listener on_mode;           // change output mode
    wl::Listener on_transform;      // the output is flipped
    wl::Listener on_damage_frame;   // frame render event
    wl::Listener on_damage_destroy; // damage object destroyed? remove output

  private:
    void render(); // Render the output

    Workspace* prev_workspace = nullptr; // For transition

    float ws_alpha = 0.f; // For transition
};

} // namespace cloth
