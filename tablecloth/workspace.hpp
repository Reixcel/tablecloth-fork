#pragma once

#include "util/chrono.hpp"
#include "util/ptr_vec.hpp"

#include "layer_shell.hpp"
#include "view.hpp"
#include "wlroots.hpp"
#include "tiling.hpp"

namespace cloth {

struct Output;
struct Desktop;

// Several workspaces in a desktop.
// Contains Views
struct Workspace
{
    Workspace(Desktop& desktop, int index)
        : desktop(desktop),
          index(index),
          tree(*this){};

    // Worksapces ordered by number
    Desktop& desktop;
    const int index;
    TileTree tree;
    View* fullscreen_view = nullptr;

    const util::ptr_vec<View>& views() const noexcept;
    util::ref_vec<View> visible_views();

    // A workspace is only visible on one output at a time
    Output* output = nullptr;

    // Is this the workspace displayed on the currently active output?
    bool is_current();
    // Is this workspace displayed on any output?
    bool is_visible();

    View* focused_view();
    View* last_focused_tile_view();
    Leaf* last_focused_tile_leaf();
    View* set_focused_view(View* view);
    View* cycle_focus();

    View& add_view(std::unique_ptr<View>&& v);
    std::unique_ptr<View> erase_view(View& v);

  private:
    util::ptr_vec<View> _views;
};

inline const util::ptr_vec<View>& Workspace::views() const noexcept
{
    return _views;
}

} // namespace cloth
