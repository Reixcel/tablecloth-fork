#include "decoration.hpp"

#include "desktop.hpp"
#include "output.hpp"
#include "view.hpp"
#include "util/logging.hpp"

#include "render.hpp"
#include "render_utils.hpp"

#include <GLES2/gl2.h>

namespace cloth {

Decoration::Decoration(View& v) : view(v) {}

void Decoration::update() {  }

void Decoration::set_visible(bool border, bool titlebar, bool shadow)
{
    if (border || titlebar || shadow) {
        visible = true;
    } else {
        if (!visible) return;
        visible = false;
    }

    // Need to damage before change in case it shrinks.
    damage();

    this->border = border;
    this->titlebar = titlebar;
    this->shadow = shadow;

    // Need to damage after change in case it has grown
    damage();
}

wlr::box_t Decoration::box() const noexcept
{
    wlr::box_t box = view.get_box();

    if (border) {
        box.x -= _border_width;
        box.y -= _border_width;
        box.width += _border_width * 2;
        box.height += (_border_width * 2);
    }

    if (titlebar) {
        box.y -= _titlebar_height;
        box.height += _titlebar_height;
    }

    return box;
}

/** Bumblefuck
 * Finds which decoration part is at the coordinates.
 * Does not include shadow.
 */
DecoPart Decoration::part_at(double sx, double sy) const noexcept
{
    if (!visible) {
        return DecoPart::none;
    }

    int sw = view.wlr_surface->current.width;
    int sh = view.wlr_surface->current.height;
    int bw; if (border) bw = _border_width; else bw = 0;
    int th; if (titlebar) th = _titlebar_height; else th = 0;
    int c_buffer = 4; // Corner buffer

    if (sx > 0 && sx < sw && sy < 0 && sy > -th && titlebar) {
        return DecoPart::titlebar;
    }

    // Tests that the coordinates are within bounds.
    // Corners are recognized on a wider area so that the cursor image
    // is not so flimsy.
    DecoPart parts = DecoPart::none;
    if (sy >= -(th + bw) && sy <= sh + bw) {
        // Test within top and bottom
        if (sx < c_buffer && sx > -bw && (sy < 0 || sy > sh)) {
            // Left corners
            parts |= DecoPart::left_border;
        } else if (sx > sw - c_buffer && sx < sw + bw && (sy < 0 || sy > sh)){
            // Right corners
            parts |= DecoPart::right_border; 
        } else if ((sx <= 0 && sx >= -bw)) {
            // Left border
            parts |= DecoPart::left_border;
        } else if (sx >= sw && sx <= sw + bw) {
            // Right border
            parts |= DecoPart::right_border;
        }
    }

    if (sx >= -bw && sx <= sw + bw) {
        // test within left and right edge
        if (sy >= -(th + bw) && sy <= -th + c_buffer && (sx <= 0 || sx >= sw)) {
            // Top corners
            parts |= DecoPart::top_border;
        } else if (sy >= sh - c_buffer  && sy <= sh + bw && (sx <=0 || sx >= sw)) {
            // Bottom corners
            parts |= DecoPart::bottom_border;
        } else if (sy >= -(th + bw) && sy <= -th) {
            // Top border
            parts |= DecoPart::top_border;
        } else if (sy >= sh && sy <= sh + bw) {
            // Bottom border
            parts |= DecoPart::bottom_border;
        }
    }

    return parts;
}

void Decoration::damage()
{
    view.damage_whole();
}

float Decoration::shadow_offset()
{
    return _shadow_offset * (view.active ? 2 : 1);
}

float Decoration::shadow_radius()
{
    return _shadow_radius * (view.active ? 2 : 1);
}

// Rendering

namespace render {

auto shadow_shader()
{
    static Shader shader = {R"END(
uniform mat3 proj;
uniform vec4 color;
attribute vec2 pos;
attribute vec2 texcoord;
varying vec4 v_color;
varying vec2 v_texcoord;
void main() {
    gl_Position = vec4(proj * vec3(pos, 1.0), 1.0);
    v_color = color;
    v_texcoord = texcoord;
}
)END",
        R"END(
precision mediump float;
varying vec4 v_color;
varying vec2 v_texcoord;
uniform float aspect;
uniform float radius;
void main()
{
vec2 pos = v_texcoord;
//pos.x /= aspect;
float dx = smoothstep(0.0, radius * aspect, min(pos.x, 1.0 - pos.x));
float dy = smoothstep(0.0, radius, min(pos.y, 1.0 - pos.y));
float alpha = dx * dy;
gl_FragColor = vec4(v_color.rgb, v_color.a * alpha);
}
)END"};
    return shader;
} 

static void draw_quad(void)
{
    GLfloat verts[] = {
        1, 0, // top right
        0, 0, // top left
        1, 1, // bottom right
        0, 1, // bottom left
    };
    GLfloat texcoord[] = {
        1, 0, // top right
        0, 0, // top left
        1, 1, // bottom right
        0, 1, // bottom left
    };

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, verts);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, texcoord);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void Context::draw_shadow(wlr::box_t box,
                          float alpha,
                          float radius,
                          float offset)
{
    //box.x += offset - radius / 2.f;
    //box.y += offset - radius / 2.f;
    //box.x += radius / 2.f;
    //box.y += radius / 2.f;
    box.height += radius;
    box.width += radius;

    pixman_region32_t damage;
    pixman_region32_init(&damage);
    pixman_region32_union_rect(&damage, &damage, box.x, box.y, box.width,
            box.height);
    pixman_region32_intersect(&damage, &damage, &pixman_damage);
    bool damaged = pixman_region32_not_empty(&damage);
    if (damaged) {
        float matrix[9];
        wlr_matrix_project_box(matrix, &box, WL_OUTPUT_TRANSFORM_NORMAL, 0,
                output.wlr_output.transform_matrix);

        int nrects;
        pixman_box32_t* rects = pixman_region32_rectangles(&damage, &nrects);
        for (int i = 0; i < nrects; ++i) {
            scissor_output(output, &rects[i]);

            float transposition[9];
            wlr_matrix_transpose(transposition, matrix);

            shadow_shader().use();

            // update the uniform color
            glUniformMatrix3fv(glGetUniformLocation(shadow_shader().ID, "proj"),
                               1,
                               false,
                               transposition);
            shadow_shader().set("color", 0.f, 0.f, 0.f, alpha);
            shadow_shader().set("aspect", box.height / (float) box.width);
            shadow_shader().set("radius", radius / (float) box.height);
            draw_quad();
        }
    }

    pixman_region32_fini(&damage);
}

static wlr::box_t get_decoration_box(View& view, Output& output)
{
    wlr::output_t& wlr_output = output.wlr_output;

    wlr::box_t deco_box = view.deco.box();

    wlr::box_t box;
    double ox = deco_box.x;
    double oy = deco_box.y;

    wlr_output_layout_output_coords(output.desktop.layout, &wlr_output, &ox, &oy);

    box.x = ox * wlr_output.scale;
    box.y = oy * wlr_output.scale;
    box.width = deco_box.width * wlr_output.scale;
    box.height = deco_box.height * wlr_output.scale;
    return box;
}


void Context::render_decoration(View& view, ViewRenderData& renderdata)
{
    if (view.maximized || view.wlr_surface == nullptr || !view.deco.visible) {
        return;
    }

    wlr::box_t box = get_decoration_box(view, output);
    double x_scale = renderdata.width / double(view.width);
    double y_scale = renderdata.height / double(view.height);
    box.x = renderdata.lx + (box.x - view.lx) * x_scale;
    box.y = renderdata.ly + (box.y - view.ly) * y_scale;
    box.width *= x_scale;
    box.height *= y_scale;

    // TODO: temporary, should be determined by state and window gap 
    draw_shadow(box, 0.5 * renderdata.alpha, view.deco.shadow_radius(), view.deco.shadow_offset());

    pixman_region32_t damage;
    pixman_region32_init(&damage);
    pixman_region32_union_rect(&damage, &damage, box.x, box.y, box.width,
            box.height);
    pixman_region32_intersect(&damage, &damage, &pixman_damage);
    bool damaged = pixman_region32_not_empty(&damage);
    if (damaged) {
        float matrix[9];
        wlr_matrix_project_box(matrix,
                               &box,
                               WL_OUTPUT_TRANSFORM_NORMAL,
                               0,
                               output.wlr_output.transform_matrix);
        std::array<float, 4> color;
        if (view.active)
            color = {0x00 / 255.f, 0x59 / 255.f, 0x73 / 255.f, renderdata.alpha};
        else
            color = {0.2, 0.2, 0.23, renderdata.alpha};

        int nrects;
        pixman_box32_t* rects = pixman_region32_rectangles(&damage, &nrects);
        for (int i = 0; i < nrects; ++i) {
            scissor_output(output, &rects[i]);

            wlr_render_quad_with_matrix(renderer, color.data(), matrix);
        }
    }

    pixman_region32_fini(&damage);
}

void Context::damage_whole_decoration(View& view)
{
    if (!view.deco.visible) {
        return;
    }

    wlr::box_t box = get_decoration_box(view, output);

    //float offset = view.deco.shadow_offset() * (view.active ? 1.f : 4.f);
    //float radius = view.deco.shadow_radius() * (view.active ? 1.f : 4.f);

    //float diff = std::min(0.f, offset - radius / 2.f);

    //box.x += diff;
    //box.y += diff;
    
    if (view.deco.shadow) {
        float radius = view.deco.shadow_radius();
        box.height += radius;
        box.width += radius;
    }

    wlr_output_damage_add_box(damage, &box);
}

} // namespace render

} // namespace cloth
