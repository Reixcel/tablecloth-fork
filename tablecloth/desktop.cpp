#include <charconv>
#include <sys/wait.h>
#include <unistd.h>


#include "util/iterators.hpp"
#include "util/exception.hpp"
#include "util/logging.hpp"

#include "layer_shell.hpp"
#include "seat.hpp"
#include "server.hpp"
#include "wlroots.hpp"
#include "xcursor.hpp"

#include "wlr-layer-shell-unstable-v1-protocol.h"

namespace cloth {

// Return the view(if any) that is visible at the layout coordinates.
View* Desktop::view_at(double lx,
                       double ly,
                       wlr::surface_t*& surface,
                       double& sx,
                       double& sy)
{
    wlr::output_t* wlr_output = wlr_output_layout_output_at(layout, lx, ly);
    if (wlr_output != nullptr) {
        Output* output = output_from_wlr_output(wlr_output);
        // If there is a fullscreen view check it
        if (output != nullptr && output->workspace->fullscreen_view != nullptr) {
            if (output->workspace->fullscreen_view->at(lx, ly, surface, sx, sy)) {
                return output->workspace->fullscreen_view;
            } else {
                return nullptr;
            }
        }

        // Else check for all visible views in output from top to bottom.
        for (auto& view : util::view::reverse(output->workspace->visible_views())) {
            if (view.at(lx, ly, surface, sx, sy))
                return &view;
        }
    }
    return nullptr;
}

Output* Desktop::output_at(double lx, double ly)
{
    wlr::output_t* wlr_output = wlr_output_layout_output_at(layout, lx, ly);
    if (wlr_output) return output_from_wlr_output(wlr_output);
    return nullptr;
}

// Return the layer_surface at outout coords and layer.
// If a layer_surface subsurface is at those coords return
// that instead.
static wlr::surface_t* layer_surface_at(Output& output,
                                        util::ptr_vec<LayerSurface>& layer,
                                        double ox,
                                        double oy,
                                        double& sx,
                                        double& sy)
{
    for (auto& surface : util::view::reverse(layer)) {
        double _sx = ox - surface.geo.x;
        double _sy = oy - surface.geo.y;

        wlr::surface_t* sub =
            wlr_layer_surface_v1_surface_at(&surface.layer_surface, _sx, _sy, &sx, &sy);

        if (sub) {
            return sub;
        }
    }
    return nullptr;
}

// Given layout coords check the layers from top to bottom
// for a surface in that location
wlr::surface_t* Desktop::surface_at(double lx,
                                    double ly,
                                    double& sx,
                                    double& sy,
                                    View*& view)
{
    wlr::surface_t* surface = nullptr;
    wlr::output_t* wlr_output = wlr_output_layout_output_at(layout, lx, ly);
    double ox = lx, oy = ly;
    view = nullptr;

    // Check if a layer surface is on either the overlay or top layer
    Output* output = nullptr;
    if (wlr_output) {
        output = (Output*)wlr_output->data;
        wlr_output_layout_output_coords(layout, wlr_output, &ox, &oy);

        if ((surface = layer_surface_at(*output,
                                        output->layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY],
                                        ox, oy, sx, sy))) {
            return surface;
        }
        if ((surface = layer_surface_at(*output,
                                        output->layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP],
                                        ox, oy, sx, sy))) {
            return surface;
        }
    }

    // Then check for views
    View* _view;
    if ((_view = view_at(lx, ly, surface, sx, sy))) {
        view = _view;
        return surface;
    }

    // Then check the bottom layer_shell layers
    if (wlr_output) {
        if ((surface = layer_surface_at(*output, 
                                        output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM],
                                        ox, oy, sx, sy))) {
            return surface;
        }
        if ((surface = layer_surface_at(*output, 
                                        output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND],
                                        ox, oy, sx, sy))) {
            return surface;
        }
    }
    return nullptr;
}

// If desktop contains the wlr_output in its
// outputs return the corresponding output
Output* Desktop::output_from_wlr_output(wlr::output_t* wlr_output)
{
    for (auto& output : outputs) {
        if (&output.wlr_output == wlr_output)
            return &output;
    }
    return nullptr;
}

// Check which views are visible in the shown workspaces
util::ref_vec<View> Desktop::visible_views()
{
    util::ref_vec<View> res;
    for (auto& ws : workspaces) {
        if (!ws.is_visible())
            continue;
        auto views = ws.visible_views();
        res.reserve(res.size() + views.size());
        util::copy(views.underlying(), std::back_inserter(res.underlying()));
    }
    return res;
}

// Return the current workspace
// When we change output it proabably pushes it to the front
// of the outputs vector
Workspace& Desktop::current_workspace()
{
    return *outputs.front().workspace;
}

// Makes a new workspace focused
void Desktop::switch_to_workspace(int idx)
{
    assert(idx >= 0 && idx < workspace_count);
    for (auto& seat : server.input.seats) {
        seat.set_focus(workspaces[idx].focused_view());
    }

    auto& output = outputs.front();
    output.workspace->output = nullptr; 
    output.workspace = &workspaces[idx];
    output.workspace->output = &output;
    output.context.damage_whole();

    // TODO: Need to move and resize view if the output is not the same as last time
    //for (auto& view : workspaces[idx].visible_views()) {
    //    view.arrange();
    //}
    
    server.workspace_manager.send_state();
    server.window_manager.send_focused_window_name(workspaces[idx]);
}

static auto execute(const char* command)
{
    auto pid = fork();
    if (pid < 0) {
        throw util::exception("Fork failed");
    }
    if (pid == 0) {
        pid = fork();
        if (pid == 0) {
            execl("/bin/sh", "/bin/sh", "-c", command, nullptr);
            _exit(EXIT_FAILURE);
        } else {
            _exit(pid == -1);
        }
    }
    int status;
    while (waitpid(pid, &status, 0) < 0) {
        if (errno == EINTR) {
            continue;
        }
        throw util::exception("waitpid() on the first child failed: {}", std::strerror(errno));
    }
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
        throw util::exception("first child failed to fork command");
    }
}
void Desktop::run_command(std::string_view command_str)
{
    Input& input = server.input;
    try {
        std::vector<std::string> args = util::split_string(std::string(command_str), " ");
        std::string command = args.at(0);
        args.erase(args.begin());

        if (command == "exit") {
            wl_display_terminate(input.server.wl_display);
        } else if (command == "close") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                focus->close();
            }
        } else if (command == "center") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                focus->center();
            }
        } else if (command == "fullscreen") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                bool is_fullscreen = focus->fullscreen_output != nullptr;
                focus->set_fullscreen(!is_fullscreen, nullptr);
            }
        } else if (command == "next_window") {
            input.server.desktop.current_workspace().cycle_focus();
        } else if (command == "alpha") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                focus->cycle_alpha();
            }
        } else if (command == "exec") {
            std::string shell_cmd = std::string(command_str.substr(strlen("exec ")));
            execute(shell_cmd.c_str());
        } else if (command == "maximize") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                focus->maximize(!focus->maximized);
            }
        } else if (command == "nop") {
            cloth_debug("nop command");
        } else if (command == "toggle_outputs") {
            outputs_enabled = !outputs_enabled;
            for (auto& output : outputs) {
                wlr_output_enable(&output.wlr_output, outputs_enabled);
            }
        } else if (command == "mark_preselect") {
            auto sel_str = args.at(0);
            if (sel_str == "left") {
                current_workspace().tree.mark_preselect(Direction::left);
            } else if (sel_str == "right") {
                current_workspace().tree.mark_preselect(Direction::right);
            } else if (sel_str == "up") {
                current_workspace().tree.mark_preselect(Direction::up);
            } else if (sel_str == "down") {
                current_workspace().tree.mark_preselect(Direction::down);
            }
        } else if (command == "toggle_tile") {
            current_workspace().tree.toggle_tile(current_workspace().focused_view());
        } else if (command == "switch_workspace") {
            int workspace = -1;
            auto ws_str = args.at(0);
            if (ws_str == "next") // +10 fixes wrapping backwards
                workspace = (input.server.desktop.current_workspace().index + 1 + 10) % 10;
            else if (ws_str == "prev")
                workspace = (input.server.desktop.current_workspace().index - 1 + 10) % 10;
            else
                std::from_chars(&*ws_str.begin(), &*ws_str.end(), workspace);
            if (workspace >= 0 && workspace < 10) {
                input.server.desktop.switch_to_workspace(workspace);
            }
        } else if (command == "move_workspace") {
            View* focus = current_workspace().focused_view();
            if (focus != nullptr) {
                int workspace = -1;
                auto ws_str = args.at(0);
                if (ws_str == "next")
                    workspace = (input.server.desktop.current_workspace().index + 1 + 10) % 10;
                else if (ws_str == "prev")
                    workspace = (input.server.desktop.current_workspace().index - 1 + 10) % 10;
                else
                    std::from_chars(&*ws_str.begin(), &*ws_str.end(), workspace);
                if (workspace >= 0 && workspace < 10) {
                    input.server.desktop.workspaces.at(workspace).add_view(
                            focus->workspace->erase_view(*focus));
                }
            }
        } else if (command == "toggle_decoration_mode") {
            View* focus = current_workspace().focused_view();
            if (auto xdg = dynamic_cast<XdgSurface*>(focus); xdg) {
                auto* decoration = xdg->xdg_toplevel_decoration.get();
                if (decoration) {
                    auto mode = decoration->wlr_decoration.current_mode;
                    mode = (mode == WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE)
                        ? WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_CLIENT_SIDE
                        : WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE;
                    wlr_xdg_toplevel_decoration_v1_set_mode(&decoration->wlr_decoration, mode);
                }
            }
        } else if (command == "rotate_output") {
            auto rotation = args.at(0);
            auto output_name = args.size() > 1 ? args.at(1) : "";
            auto output = util::find_if(input.server.desktop.outputs,
                    [&](Output& o) { return o.wlr_output.name == output_name; });
            if (output == input.server.desktop.outputs.end())
                output = input.server.desktop.outputs.begin();
            auto transform = [&] {
                if (rotation == "0") return WL_OUTPUT_TRANSFORM_NORMAL;
                if (rotation == "90") return WL_OUTPUT_TRANSFORM_90;
                if (rotation == "180") return WL_OUTPUT_TRANSFORM_180;
                if (rotation == "270") return WL_OUTPUT_TRANSFORM_270;
                throw util::exception("Invalid rotation. Expected 0,90,180 or 270. Got {}", rotation);
            }();
            wlr_output_set_transform(&output->wlr_output, transform);
        } else {
            cloth_debug("unknown binding command: {}", command);
        }
    } catch (std::exception& e) {
        cloth_debug("Error running command: {}", e.what());
    }
}


Desktop::Desktop(Server& p_server, Config& p_config) noexcept
    : workspaces(util::generate_array<10>(
                    [this](int n) { return Workspace(*this, n); })),
      server(p_server),
      config(p_config)
{
    cloth_debug("Initializing tablecloth desktop");

    on_new_output = [this](void* data) {
        cloth_debug("New output");
        // TODO: Should be set to the first open workspace, or create a new one if necessary
        outputs.emplace_back(*this, workspaces[0], *(wlr::output_t*)data);

        for (auto& seat : server.input.seats) {
            seat.configure_cursor();
            seat.configure_xcursor();
        }
    };
    on_new_output.add_to(server.backend->events.new_output);

    layout = wlr_output_layout_create();
    wlr_xdg_output_manager_v1_create(server.wl_display, layout);

    on_layout_change.add_to(layout->events.change);
    on_layout_change = [this](void* data) {
        wlr::output_t* center_output = wlr_output_layout_get_center_output(this->layout);
        if (center_output == nullptr)
            return;

        wlr::box_t* center_output_box =
            wlr_output_layout_get_box(this->layout, center_output);
        double center_x = center_output_box->x + center_output_box->width / 2;
        double center_y = center_output_box->y + center_output_box->height / 2;

        for (auto& output : outputs) {
            for (auto& view : output.workspace->views()) {
                auto box = view.get_box();
                if (wlr_output_layout_intersects(this->layout, nullptr, &box))
                    continue;
                view.move(center_x - box.width / 2, center_y - box.height / 2);
            }
        }
    };

    compositor = wlr_compositor_create(server.wl_display, server.renderer);

    xdg_shell_v6 = wlr_xdg_shell_v6_create(server.wl_display);
    on_xdg_shell_v6_surface = [this](void* data) { handle_xdg_shell_v6_surface(data); };
    on_xdg_shell_v6_surface.add_to(xdg_shell_v6->events.new_surface);

    xdg_shell = wlr_xdg_shell_create(server.wl_display);
    on_xdg_shell_surface.add_to(xdg_shell->events.new_surface);
    on_xdg_shell_surface = [this](void* data) { handle_xdg_shell_surface(data); };

    wl_shell = wlr_wl_shell_create(server.wl_display);
    on_wl_shell_surface.add_to(wl_shell->events.new_surface);
    on_wl_shell_surface = [this](void* data) { handle_wl_shell_surface(data); };

    layer_shell = wlr_layer_shell_v1_create(server.wl_display);
    on_layer_shell_surface.add_to(layer_shell->events.new_surface);
    on_layer_shell_surface = [this](void* data) { handle_layer_shell_surface(data); };

    tablet_v2 = wlr_tablet_v2_create(server.wl_display);

#ifdef WLR_HAS_XWAYLAND
    const char* cursor_theme = nullptr;
    const char* cursor_default = xcursor_default;
    Config::Cursor* cc = config.get_cursor(Config::default_seat_name);
    if (cc != nullptr) {
        cursor_theme = cc->theme.c_str();
        if (!cc->default_image.empty()) {
            cursor_default = cc->default_image.c_str();
        }
    }

    xcursor_manager = wlr_xcursor_manager_create(cursor_theme, xcursor_size);
    if (xcursor_manager == nullptr) {
        cloth_error("Cannot create XCursor manager for theme {}", cursor_theme);
    }

    if (config.xwayland) {
        xwayland =
            wlr_xwayland_create(server.wl_display, compositor, config.xwayland_lazy);
        on_xwayland_surface.add_to(xwayland->events.new_surface);
        on_xwayland_surface = [this](void* data) { handle_xwayland_surface(data); };

        if (wlr_xcursor_manager_load(xcursor_manager, 1)) {
            cloth_error("Cannot load XWayland XCursor theme");
        }
        wlr::xcursor_t* xcursor =
            wlr_xcursor_manager_get_xcursor(xcursor_manager, cursor_default, 1);
        if (xcursor != nullptr) {
            wlr::xcursor_image_t* image = xcursor->images[0];
            wlr_xwayland_set_cursor(xwayland,
                                    image->buffer,
                                    image->width * 4,
                                    image->width,
                                    image->height,
                                    image->hotspot_x,
                                    image->hotspot_y);
        }
    }
#endif

    gamma_control_manager = wlr_gamma_control_manager_create(server.wl_display);
    gamma_control_manager_v1 = wlr_gamma_control_manager_v1_create(server.wl_display);
    screenshooter = wlr_screenshooter_create(server.wl_display);
    export_dmabuf_manager_v1 = wlr_export_dmabuf_manager_v1_create(server.wl_display);
    server_decoration_manager = wlr_server_decoration_manager_create(server.wl_display);
    wlr_server_decoration_manager_set_default_mode(
        server_decoration_manager, WLR_SERVER_DECORATION_MANAGER_MODE_SERVER);
    on_server_decoration.add_to(server_decoration_manager->events.new_decoration);
    on_server_decoration = [](void* data) {
        auto* decoration = (wlr::server_decoration_t*)data;
        // set in View::map and View::unmap
        auto* view = (View*)decoration->surface->data;
        if (view && decoration->mode == WLR_SERVER_DECORATION_MANAGER_MODE_SERVER) {
            cloth_debug("Updated server decoration for view");
            view->deco.set_visible();
        } else {
            wlr_log(WLR_DEBUG,"No view found for server decoration");
        }
    };

    primary_selection_device_manager =
        wlr_primary_selection_device_manager_create(server.wl_display);
    idle = wlr_idle_create(server.wl_display);
    idle_inhibit = wlr_idle_inhibit_v1_create(server.wl_display);

    input_inhibit = wlr_input_inhibit_manager_create(server.wl_display);
    on_input_inhibit_activate = [this](void* data) {
        for (auto& seat : this->server.input.seats) {
            seat.set_exclusive_client(this->input_inhibit->active_client);
        }
    };
    on_input_inhibit_activate.add_to(input_inhibit->events.activate);

    on_input_inhibit_deactivate = [this](void* data) {
        for (auto& seat : this->server.input.seats) {
            seat.set_exclusive_client(nullptr);
        }
    };
    on_input_inhibit_deactivate.add_to(input_inhibit->events.deactivate);

    linux_dmabuf = wlr_linux_dmabuf_v1_create(server.wl_display, server.renderer);

    virtual_keyboard = wlr_virtual_keyboard_manager_v1_create(server.wl_display);

    on_virtual_keyboard_new = [this](void* data) {
        cloth_debug("New virutal keyboard");
        auto& keyboard = *(wlr::virtual_keyboard_v1_t*)data;

        auto* seat = this->server.input.seat_from_wlr_seat(*keyboard.seat);
        if (!seat) {
            cloth_debug("could not find tablecloth seat");
            return;
        }

        seat->add_device(keyboard.input_device);
    };
    on_virtual_keyboard_new.add_to(virtual_keyboard->events.new_virtual_keyboard);

    screencopy = wlr_screencopy_manager_v1_create(server.wl_display);

    // TODO: Will enable when I find out how it works,
    // the xdg shells I've used use the server_decoration_manager instead
    //xdg_decoration_manager_v1 = wlr_xdg_decoration_manager_v1_create(server.wl_display);
    //on_xdg_toplevel_decoration = [this](void* data) {
    //    handle_xdg_toplevel_decoration(data);
    //};
    //on_xdg_toplevel_decoration.add_to(
    //    xdg_decoration_manager_v1->events.new_toplevel_decoration);
}

Desktop::~Desktop() noexcept
{
    // TODO
}
} // namespace cloth
