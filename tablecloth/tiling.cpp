#include <algorithm>

#include "util/logging.hpp"
#include "wlroots.hpp"

#include "tiling.hpp"
#include "output.hpp"
#include "desktop.hpp"
#include "workspace.hpp"

namespace cloth {

static Split direction_to_split(Direction dir)
{
    if (dir == Direction::left || dir == Direction::right) {
        return Split::horiz;
    } else {
        return Split::vert;
    }
}

std::shared_ptr<Leaf>& Leaf::add_child(util::shared_ptr_vec<Leaf>::iterator it)
{
    Leaf leaf = Leaf(this);
    auto new_leaf_it = children.insert(it, leaf);
    return *new_leaf_it;
}

auto Leaf::remove() -> std::shared_ptr<Leaf> 
{
    auto keep_alive = this->parent->children.erase(*this);
    return keep_alive;
}

auto Leaf::location() -> util::shared_ptr_vec<Leaf>::iterator
{
    auto child_it = std::find_if(this->parent->children.begin(),
                                 this->parent->children.end(),
                                 [this](auto& leaf) { return this == leaf.get(); });
    return child_it;
}

bool Leaf::is_child()
{
    if (parent == nullptr) {
        // root leaf
        return false;
    } else {
        // Only parents have a split direction
        return split == Split::none;
    }
}

TileTree::TileTree(Workspace& ws)
    : workspace(ws)
{
    _preselect = _root.add_child(_root.children.begin());
    _leaves.push_back(_preselect);
}

/**
 * Adds a child leaf (for a preselect or a view) to parent at position 'it'.
 * Cannot make a new leaf unless there's space for it.
 * Measures the ratio between the amputee's current width/height 
 * compared to its minimal possible width/height.
 * Larger than 2 means it can be split in half,
 * Between 2 and 1 means it has to take up less than half the space,
 * Less than 1 means it can't split
 * If the 'amputee' can't split, or the 'view' minimum size is too large,
 * the leaf will not be created.
 * Returns true if the leaf was created.
 */
bool TileTree::add_leaf(Leaf& parent, 
                        Leaf& amputee, 
                        int location,
                        View* view)
{
    if (parent.is_child()) {
        // Convert it into a parent by squizing a new leaf in-between
        // Is converted back into a child if the function fails
        auto it = parent.location();
        auto sptr = std::move(*it);
        *it = std::make_shared<Leaf>(Leaf(parent.parent));

        // Transplant leaf vaues to new parent
        sptr->parent = (*it).get();
        sptr->parent->area = sptr->area;
        sptr->parent->split_ratio = sptr->split_ratio;
        // Reset leaf values
        sptr->split_ratio = 1;

        (*it)->children.push_back(std::move(sptr));

        if ((*it)->parent->split == Split::horiz) {
            (*it)->split = Split::vert;
        } else {
            (*it)->split = Split::horiz;
        }
    }

    auto& new_leaf_ptr = amputee.parent->add_child(amputee.location()+location);

    // How many percent amputee can be resized by before becoming too small.
    float compare;
    bool vert = amputee.parent->split == Split::vert;
    if (vert) {
        compare = amputee.area.height / amputee.view->minimal_area().height;
    } else {
        compare = amputee.area.width / amputee.view->minimal_area().width;
    }

    // Modify 'area' to get an approximate that can be used before arrange (TODO bad idea?),
    // will be overwritten before the function returns
    if (compare > 2.f) {
        amputee.split_ratio /= 2.f;
        new_leaf_ptr->split_ratio = amputee.split_ratio;
        if (vert) {
            new_leaf_ptr->area.height = amputee.area.height / 2;
            new_leaf_ptr->area.width = amputee.area.width;
        } else {
            new_leaf_ptr->area.width = amputee.area.width / 2;
            new_leaf_ptr->area.height = amputee.area.height;
        }
    } else if (compare > 1.f) {
        // TODO: Maybe it should be 1.1 to give it a buffer to not fuck up
        float remainder = amputee.split_ratio - amputee.split_ratio/compare;
        amputee.split_ratio -= remainder;
        new_leaf_ptr->split_ratio = remainder;
        if (vert) {
            new_leaf_ptr->area.height = amputee.area.height * remainder;
            new_leaf_ptr->area.width = amputee.area.width;
        } else {
            new_leaf_ptr->area.width = amputee.area.width * remainder;
            new_leaf_ptr->area.height = amputee.area.height;
        }
    } else {
        remove_leaf(*new_leaf_ptr);
        cloth_debug("Leaf is too small to be split further");
        return false;
    }

    // Fill or make a preselection
    if (view) {
        _preselect = new_leaf_ptr;

        bool success = fill_preselect(view);
        if (!success) {
            _preselect = nullptr;
            remove_leaf(*new_leaf_ptr);
            return false;
        }
    } else {
        cloth_debug("Creating new preselect");
        _preselect = new_leaf_ptr;
    }

    _leaves.push_back(new_leaf_ptr); // Delayed to not have to erase when it fails
    //arrange(*child.parent);
    arrange_root();
    return true;
}

/**
 * Removes the leaf from its parent.
 * If the parent only has one child leaf left in its children, convert it into a child.
 * If the parent only has one parent leaf left in its children, move the children of
 * the leaf to the parent's parent to retain the split.
 * If the parent is the root leaf
 * Returns the leaf in case it should be put somewhere else.
 */
auto TileTree::remove_leaf(Leaf& child) -> std::shared_ptr<Leaf>
{
    auto parent = child.parent;

    // If the root leaf only has one child it shouldn't be deleted.
    // It is converted back into a preselect.
    if (parent == &_root && parent->children.size() == 1) {
        child.view = nullptr;
        _preselect = parent->children[0];
        return parent->children[0];
    }

    _leaves.erase(child);
    auto keep_alive = child.remove();

    // split for '_root' will remain. It always resets when 
    // adding a new leaf with only one child.
    if (parent->children.size() == 1 && parent != &_root) {
        if (parent->children[0]->is_child()) {
            // Transplant the view of the last child
            parent->view = parent->children[0]->view;
            // Reset split direction
            parent->split = Split::none;
            // Kill the kid
            _leaves.erase(parent->children[0]);
            parent->children.clear();
            // Adopt new one
            _leaves.push_back(*parent->location());
        } else {
            // Move all child leaves up two levels so the split remains the same.
            for (auto& child : parent->children[0]->children) {
                child->parent = parent->parent;
                child->split_ratio = parent->split_ratio * child->split_ratio;
            }
            // Move construct(?) all the children
            parent->parent->children.insert(
                    parent->parent->children.end(),
                    parent->children[0]->children.begin(),
                    parent->children[0]->children.end());
            // Remove the old parents
            parent->parent->children.erase(parent->location());
        }
    } else {
        // Increase the split_ratio of every child by an equal amount
        // If 'keep_alive' is the last leaf of '_root' this won't do anything
        float ratio = 
            keep_alive->split_ratio/keep_alive->parent->children.size();
        for (auto& child : keep_alive->parent->children) {
            child->split_ratio += ratio;
        }
    }
    arrange_root();
    return keep_alive;
}

/**
 * Return the leaf that is at the coordinates, if there is one.
 * Will also return if the leaf is fullscreened/maximized.
 */
Leaf* TileTree::leaf_at(double lx, double ly)
{
    // TODO: The fullscreen part with _special_leaf
    for (auto& leaf : _leaves) {
        if(lx >= leaf->area.x && lx <= leaf->area.width + leaf->area.x &&
           ly >= leaf->area.y && ly <= leaf->area.height + leaf->area.y) {
            return leaf.get();
        }
    }
    return nullptr;
}

/**
 * Return a pointer to the leaf containing view, if it exists.
 */
auto TileTree::leafptr_from_view(View* view) -> std::shared_ptr<Leaf>
{
    auto it = std::find_if(
            _leaves.begin(),
            _leaves.end(),
            [view](auto& leaf) { return leaf->view == view; });

    if (it == _leaves.end()) {
        return nullptr;
    } else {
        return *it;
    }
}

int TileTree::depth(Leaf* leaf)
{
    int depth = 0;
    while (leaf->parent != nullptr) {
        leaf = leaf->parent;
        ++depth;
    }
    return depth;
}

/**
 * Find the the relative of 'first' and 'second' that are siblings.
 */
Leaf* TileTree::sibling(Leaf*& first, Direction dir)
{
    Leaf* second = leaf_in_dir(*first, dir);
    if (!second) return nullptr;
    
    int first_depth = depth(first);
    int second_depth = depth(second);

    while (first->parent != second->parent) {
        if (first_depth > second_depth) {
            first = first->parent;
            --first_depth;
        } else if (second_depth > first_depth) {
            second = second->parent;
            --second_depth;
        } else {
            first = first->parent;
            second = second->parent;
        }
    }
    return second;
}

// NOT IN USE
//static float find_size(Leaf& child) 
//{
//    //return if child of root
//    if(child.parent->parent == nullptr) {
//        return child.split_ratio;
//    } else {
//        return (child.split_ratio) * find_size(*child.parent);
//    }
//}
//
//Leaf& TileTree::largest_leaf() 
//{
//    float largest_size = 0;
//    Leaf* big_leaf;
//
//    for (auto child : _leaves) {
//        float size = find_size(*child);
//        if (size > largest_size) {
//            big_leaf = child.get();
//            largest_size = size;
//        }
//    }
//
//    return *big_leaf;
//}

/**
 * In direction 'dir' return the closest leaf that shares a common
 * edge with leaf.  This can be a leaf at any depth in the tree.
 * Returns nullptr if it is the edge of the tiled area.
 */
Leaf* TileTree::leaf_in_dir(Leaf& leaf, Direction dir)
{
    // TODO: Add gap
    // TODO: Return nothing if fullsceen/monocle and probably some more states?
    double lx, ly;
    switch(dir) {
        case Direction::up:
            lx = leaf.area.x + leaf.area.width / 2;
            ly = leaf.area.y - 5; 
            break;
        case Direction::left:
            lx = leaf.area.x - 5;
            ly = leaf.area.y + leaf.area.height / 2;
            break;
        case Direction::down:
            lx = leaf.area.x + leaf.area.width / 2;
            ly = leaf.area.y + leaf.area.height + 5; 
            break;
        case Direction::right:
            lx = leaf.area.x + leaf.area.width + 5;
            ly = leaf.area.y + leaf.area.height / 2;
            break;
    }

    Leaf* dir_leaf = leaf_at(lx, ly);
    return dir_leaf;
}

/**
 * Return the area to be used for tiling, after outer gap has been applied
 */
wlr::box_t TileTree::usable_area()
{
    wlr::box_t area = workspace.output->usable_area;
    return area;
}

void TileTree::arrange_root() 
{
    auto area = usable_area();

    if (layout == Layout::tiling) {
        // TODO outer gap
        arrange(_root, area);
    } else if (layout == Layout::fullscreen) {
        // TODO When going out of fullscreen it needs to know how to get back
    } else if (layout == Layout::monocle) {
        // TODO Same as above
    }
}

void TileTree::arrange(Leaf& leaf, wlr::box_t area)
{
    //TODO padding  and window gap stuff
    leaf.area = area;
    if (!leaf.is_child()) {
        std::vector<wlr::box_t> child_areas;
        child_areas.reserve(leaf.children.size());

        wlr::box_t child_area = area;
        for (auto& child : leaf.children) {
            wlr::box_t min;
            if (child.view) {
                min = child.view->minimal_area();
            } else {
            }
        }

        // Keep track of position of the last child
        for (auto& child : leaf.children) {
            wlr::box_t child_area = area; 
            if (leaf.split == Split::none) {
                // '_root' with 1 child
                arrange(*child,child_area);
            } else if (leaf.split == Split::horiz) {
                child_area.width *= child->split_ratio;
                arrange(*child, child_area);
                area.x += child_area.width;
            } else if (leaf.split == Split::vert) {
                child_area.height *= child->split_ratio;
                arrange(*child, child_area);
                area.y += child_area.height;
            }
        }
    } else {
        if (!leaf.view){
            // TODO: Plaster up preselect placeholder
            return;
        }
        // Confines border and surface within area
        leaf.view->move_resize_with_deco(area);
    }
}

/**
 * Check if you can decrease the size of 
 * leaf's view down to 1 - 'percent' percent.
 * e.g to 90% of its current size if 'percent' is 0.1. 
 * If it hits a size restriction it will return
 * the max percentage it can be resized by.
 * 'split' is the direction the view is being resized.
 */
float TileTree::check_resize(Leaf& leaf, Split split, float percent)
{
    float constrained_percent = percent;

    if (percent < 0) {
        cloth_error("Tried to apply size contraints to a window that was growing");
        return percent;
    } else if (!leaf.is_child()) {
        // Parents need to check for all their children in the direction given.
        float size = leaf.children.size();
        constrained_percent = 0;
        for (auto& child : leaf.children) {
            constrained_percent += check_resize(*child, split, percent) / size;
        }
    } else if (split == Split::vert){
        // Check for height constraints.
        int dest_width, dest_height;
        int new_height = leaf.area.height * (1 - percent);
        leaf.view->apply_size_constraints(
                leaf.area.width, new_height, dest_width, dest_height);
        if (new_height != dest_height) {
            constrained_percent = 1 - float(dest_height) / float(leaf.area.height);
        }
    } else if (split == Split::horiz) {
        // Check for width constraints.
        int dest_width, dest_height;
        int new_width = leaf.area.width * (1 - percent);
        leaf.view->apply_size_constraints(
                new_width, leaf.area.height, dest_width, dest_height);
        if (new_width != dest_width) {
            constrained_percent = 1 - float(dest_width) / float(leaf.area.width);
        }
    }
    return constrained_percent;
}

/** 
 * Give or take 'abs_percent'% from 'brother' to 'sister'.
 * Will resize by less than 'pixels' if any of the views in the leaf
 * has a size restriction.
 */
void TileTree::resize_leaves(Leaf& brother, Leaf& sister, float abs_percent)
{
    // Has to be siblings
    if (brother.parent != sister.parent) return;

    if (abs_percent == 0) {
        // This happens when moving the cursor orthogonally to the split direction
        return;
    }
    if (abs_percent < 0) {
        // Resized window gets smaller
        // The ratio is how many percent it should shrink
        float relative_percent = -abs_percent / brother.split_ratio;
        // Cap 'relative_percent' to where the view hits a constraint.
        float capped_percent = check_resize(brother, brother.parent->split, relative_percent);
        // Scale the relative percentage again and apply it
        abs_percent = capped_percent * brother.split_ratio; 
        sister.split_ratio += abs_percent;
        brother.split_ratio -= abs_percent;
    } else {
        // resized window get bigger
        float relative_percent = abs_percent / sister.split_ratio;
        // 'abs_percent' can be more that the split ratio.
        relative_percent = relative_percent > 1 ? 1 : relative_percent;

        float capped_percent = check_resize(sister, sister.parent->split, relative_percent);
        abs_percent = capped_percent * sister.split_ratio; 
        brother.split_ratio += abs_percent;
        sister.split_ratio -= abs_percent;
    }
    arrange_root();
}

/**
 * Resize the leaf in the given direction by 'percent'%.
 * If there is no sibling of leaf in the direction
 * it finds the closest leaf in that direction and resizes
 * the nearest direct relatives of the two leaves.
 */
void TileTree::resize_tile(Leaf& leaf, Direction dir, float percent)
{
    Leaf* first_sibling = &leaf;
    Leaf* second_sibling = sibling(first_sibling, dir);
    if (!second_sibling) return; // No leaf in that direction must mean it's an edge

    resize_leaves(*first_sibling, *second_sibling, percent);
}

void TileTree::resize_tile(Leaf& leaf, Direction dir, int pixels)
{
    Leaf* first_sibling = &leaf;
    Leaf* second_sibling = sibling(first_sibling, dir);
    if (!second_sibling) return; // No leaf in that direction must mean it's an edge

    // How many percent the pixels amount to of the total width/height of the parent.
    float percent;
    if (Split::vert == direction_to_split(dir)) {
        percent = float(pixels) / float(first_sibling->parent->area.height);
    } else {
        percent = float(pixels) / float(first_sibling->parent->area.width);
    }
    
    resize_leaves(*first_sibling, *second_sibling, percent);
}

/**
 * This is used for mouse resizing.
 * If the variables provided don't match with the stored ones it will trigger
 * a resize.
 */
void TileTree::resize_tile(Leaf& leaf, double lx, double ly, uint32_t edges)
{
    // BUG: Can't trust the correct parameters to be changed for the different edges
    // so it checks the edge instead. I noticed this when resizing the left edge,
    // sometimes lx is not changed while width is, indicating a right edge resize.
    if (edges & WLR_EDGE_TOP) {
        // Moving top border
        int pixels = leaf.area.y - ly;
        resize_tile(leaf, Direction::up, pixels);
    } else if (edges & WLR_EDGE_BOTTOM) {
        // Moving bottom border
        int pixels = ly - (leaf.area.height + leaf.area.y);
        resize_tile(leaf, Direction::down, pixels);
    }
    if (edges & WLR_EDGE_LEFT) {
        // Moving left border
        int pixels = leaf.area.x - lx;
        resize_tile(leaf, Direction::left, pixels);
    } else if (edges & WLR_EDGE_RIGHT) {
        // Moving right border
        int pixels = lx - (leaf.area.width + leaf.area.x);
        resize_tile(leaf, Direction::right, pixels);
    }
}

/**
 * Views can be toggled between floating and tiled.
 * If there is no active preselect, the view will create a new leaf
 * splitting the last focused one.
 * Untiling a view will not reserve its spot in the tiling tree.
 * If it is immediately tiled again without a preselect, it will
 * start over.
 */
void TileTree::toggle_tile(View* view)
{
    // Currently this function is only used with the focused view
    // which can pass nullptr if there are no views
    if (!view) {
        cloth_debug("Tried to toggle tiling mode for non existing view");
        return;
    }

    if (view->state == ViewState::tiled || view->state == ViewState::pseudo_tiled) {
        // Remove the leaf and convert it to floating again
        auto view_leaf = leafptr_from_view(view);
        if (!view_leaf) {
            cloth_info("THIS IS BAD"); // If the state is tiled there should always be a corresponding leaf
            return;
        }
        remove_leaf(*view_leaf);
        view->move_resize(view->saved);
        view->set_state(ViewState::floating);
    } else if (!_preselect && _root.children.size() == 1){
        // Need to set the split dir for the root node.
        // Splits the longest side
        Leaf& split_leaf = *_root.children[0];
        auto box = split_leaf.view->get_box();

        if (box.width > box.height) {
            _root.split = Split::horiz;
        } else {
            _root.split = Split::vert;
        }

        add_leaf(*split_leaf.parent, split_leaf, 1, view);
    } else if (!_preselect) {
        // Add a new leaf to the right or below the last leaf that had focus
        Leaf& split_leaf = *workspace.last_focused_tile_leaf();
        add_leaf(split_leaf, split_leaf, 1, view);
    } else {
        fill_preselect(view);
    }
}

void TileTree::mark_preselect(Direction dir)
{
    // This covers not being able to create a preselect if '_root' doesn't
    // have any children.
    auto childptr = workspace.last_focused_tile_leaf();
    if (!childptr) {
        cloth_debug("No focused leaf could be found, skipped preselection");
        return;
    }
    auto& child = *childptr;

    // 'none' split means it's the root leaf with one child
    // Same split creates a new child
    // Opposite split splits into new parent
    if (child.parent->split == Split::none) {
        child.parent->split = direction_to_split(dir);
        if (dir == Direction::left || dir == Direction::up) {
            add_leaf(*child.parent, child, 0, nullptr);
        } else {
            add_leaf(*child.parent, child, 1, nullptr);
        }
    } else if (direction_to_split(dir) == child.parent->split) {
        if (dir == Direction::left || dir == Direction::up) {
            add_leaf(*child.parent, child, 0, nullptr);
        } else {
            add_leaf(*child.parent, child, 1, nullptr);
        }
    } else if (direction_to_split(dir) != child.parent->split) {
        if (dir == Direction::left || dir == Direction::up) {
            add_leaf(child, child, 0, nullptr);
        } else {
            add_leaf(child, child, 1, nullptr);
        }
    }
}

bool TileTree::fill_preselect(View* view)
{
    if (!_preselect){
        cloth_debug("Tried to fill non-existing preselect");
        return false;
    } 

    wlr::box_t view_area = view->minimal_area();
    if ((view_area.width >= _preselect->area.width ||
            view_area.height >= _preselect->area.height) &&
            _preselect != _root.children[0]) {
        cloth_debug("{} cannot be made small enough to fit the tile", view->get_name());
        return false;
    }

    cloth_debug("Setting {} to tiled mode", view->get_name());
    _preselect->view = view;
    view->save();
    view->set_state(ViewState::tiled);
    _preselect = nullptr;
    arrange_root();
    return true;
}

bool TileTree::active_preselect()
{
    if (_preselect == nullptr ||
            _preselect == _root.children[0]) {
        return false;
    } else {
        return true;
    }
}

} // namespace cloth
