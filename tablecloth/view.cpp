#include "view.hpp"

#include "util/algorithm.hpp"
#include "util/logging.hpp"

#include "desktop.hpp"
#include "layer_shell.hpp"
#include "seat.hpp"
#include "server.hpp"
#include "wlroots.hpp"
#include "workspace.hpp"
#include "xcursor.hpp"

#include "wlr-layer-shell-unstable-v1-protocol.h"

namespace cloth {

View::View(Workspace& workspace)
    : workspace(&workspace),
      desktop(workspace.desktop),
      input(desktop.server.input)
{}

View::~View() noexcept
{
    remove_seat_focus();

    if (this->state == ViewState::tiled || this->state == ViewState::pseudo_tiled) {
        workspace->tree.remove_leaf(*workspace->tree.leafptr_from_view(this));
    }

    if (wlr_surface)
        unmap();
}

// Check if the view is in focus on the assigned workspace.
bool View::is_focused()
{
    return workspace->focused_view() == this;
}

bool View::is_tiled()
{
    // Not testing for '!= ViewState::floating' cause might add more states later
    if (state == ViewState::tiled || state == ViewState::pseudo_tiled) {
        return true;
    }
    return false;
}

// Check for subsurfaces or popus
bool View::is_standalone()
{
    if (!wl_list_empty(&wlr_surface->subsurfaces)) {
        return false;
    }

    if (auto* xdg_surface_v6 = dynamic_cast<XdgSurfaceV6*>(this); xdg_surface_v6) {
        return wl_list_empty(&xdg_surface_v6->xdg_surface->popups);
    } else if (auto* xdg_surface = dynamic_cast<XdgSurface*>(this); xdg_surface) {
        return wl_list_empty(&xdg_surface->xdg_surface->popups);
    } else if (auto* wl_shell_surface = dynamic_cast<WlShellSurface*>(this);
               wl_shell_surface) {
        return wl_list_empty(&wl_shell_surface->wl_shell_surface->popups);
    }
#ifdef WLR_HAS_XWAYLAND
    if (auto* wlr_surface = dynamic_cast<XwaylandSurface*>(this); wlr_surface) {
        return wl_list_empty(&wlr_surface->xwayland_surface->children);
    }
#endif
    return true;
}

// Get box of view with layout coords
// TODO: Why should width and height be ints?
wlr::box_t View::get_box() const
{
    return { .x = (int)lx,
             .y = (int)ly,
             .width = (int)width,
             .height = (int)height };
}

// No constraints
void View::apply_size_constraints(int width, int height, int& dest_width, int& dest_height)
{
    dest_width = width;
    dest_height = height;
}

// Signifies that the view can be as small as need be
wlr::box_t View::minimal_area()
{
    wlr::box_t area;
    apply_size_constraints(1, 1, area.width, area.height);
    return area;
}


// If view was in one output but no longer is send a leave event,
// else if it wasn't but now is send an enter event.
void View::update_output(std::optional<wlr::box_t> before) const
{
    if (wlr_surface == nullptr)
        return;
    auto box = get_box();

    for (auto& output : desktop.outputs) {
        bool intersected = 
            before.has_value() &&
            wlr_output_layout_intersects(desktop.layout,
                                         &output.wlr_output,
                                         &before.value());
        bool intersects =
            wlr_output_layout_intersects(desktop.layout, &output.wlr_output, &box);

        if (intersected && !intersects) {
            wlr_surface_send_leave(wlr_surface, &output.wlr_output);
        } else if (!intersected && intersects) {
            wlr_surface_send_enter(wlr_surface, &output.wlr_output);
        }
    }
}

// Same as update_position.
// Move view to new coords.
void View::do_move(double lx, double ly)
{
    update_position(lx, ly);
}

// Update position already checks for same coords.
// Move the view and make sure it knows which output it's in.
void View::move(double lx, double ly)
{
    if (this->lx == lx && this->ly == ly) {
        return;
    }

    do_move(lx, ly);
}

void View::remove_seat_focus()
{
    for (Seat& seat : input.seats) {
        if (this == seat.get_focus()) {
            seat.set_focus(nullptr);
        }
    }
}

// Alert client it is / also used for SSD
void View::activate(bool activate)
{
    active = activate;
    do_activate(activate);
    deco.damage();
}

// Resize the view
void View::resize(int width, int height)
{
    auto before = get_box();
    do_resize(width, height);
    update_output(before);
}

void View::do_move_resize(double lx, double ly, int width, int height)
{
    pending_move_resize.update_x = lx != this->lx;
    pending_move_resize.update_y = ly != this->ly;
    pending_move_resize.x = lx;
    pending_move_resize.y = ly;
    pending_move_resize.width = width;
    pending_move_resize.height = height;
}

// Perform a move_resize.
// If coords don't change, only resize
void View::move_resize(double x, double y, int width, int height)
{
    bool update_x = x != this->lx;
    bool update_y = y != this->ly;
    if (update_x || update_y) {
        do_move_resize(x, y, width, height);
    } else {
        resize(width, height);
    }
}

void View::move_resize(const wlr::box_t& box)
{
    move_resize(box.x, box.y, box.width, box.height);
}

void View::move_resize_with_deco(const wlr::box_t& box) 
{
    move_resize(
            box.x + deco.border_width(),
            box.y + deco.border_width(),
            box.width - 2 * deco.border_width(),
            box.height - 2 * deco.border_width());
}

// Get the output where the middle of the view is.
Output* View::get_output()
{
    double output_lx, output_ly;
    wlr_output_layout_closest_point(desktop.layout,
                                    nullptr,
                                    lx + width / 2,
                                    ly + height / 2,
                                    &output_lx,
                                    &output_ly);
    return desktop.output_at(output_lx, output_ly);
}

// Perform the maximizing
void View::maximize(bool maximized)
{
    if (maximized) {
        auto& output = *get_output();

        auto* output_box = wlr_output_layout_get_box(desktop.layout, &output.wlr_output);
        auto usable_area = output.usable_area;

        usable_area.x += output_box->x;
        usable_area.y += output_box->y;

        if (!this->maximized) {
            // Maximize if not maximized
            this->maximized = true;
            save();
            move_resize(usable_area);
            do_maximize(true);
        } else if (this->maximized){
            // Used when rearranging, causing the usable_area to be less
            move_resize(usable_area);
        }
    }

    if (this->maximized && !maximized) {
        this->maximized = false;
        move_resize(saved);
    }
}

// Set view to fullscreen, covering exclusive zones
void View::set_fullscreen(bool fullscreen, wlr::output_t* wlr_output)
{
    bool was_fullscreen = this->fullscreen_output != nullptr;

    if (fullscreen) {
        Output* output = nullptr;
        if (wlr_output == nullptr) {
            output = get_output();
        } else {
            output = desktop.output_from_wlr_output(wlr_output);
        }

        // New fullscreen
        // Else: change fullscreen output
        if (!was_fullscreen) {
            save();

            auto& output_box = *wlr_output_layout_get_box(desktop.layout, &output->wlr_output);
            move_resize(output_box);

            output->workspace->fullscreen_view = this;
            fullscreen_output = output;
            output->context.damage_whole();
            do_set_fullscreen(fullscreen);
        } else if (was_fullscreen) {
            auto& output_box = *wlr_output_layout_get_box(desktop.layout, &output->wlr_output);
            move_resize(output_box);

            output->workspace->fullscreen_view = this;
            fullscreen_output = output;
        }
    }

    if (was_fullscreen && !fullscreen) {
        do_set_fullscreen(fullscreen);
        move_resize(saved);
        fullscreen_output->workspace->fullscreen_view = nullptr;
        fullscreen_output = nullptr;
    }
}

void View::set_state(ViewState state) 
{
    if (state == ViewState::tiled) {
        deco.set_visible(true,false);
        this->state = state;
    } else if (state == ViewState::floating) {
        deco.set_visible();
        this->state = state;
    }
}

// Make transparent
void View::cycle_alpha()
{
    alpha -= 0.05;
    /* Don't go completely transparent */
    if (alpha < 0.1) {
        alpha = 1.0;
    }
    damage_whole();
}

void View::close()
{
    do_close();
}

// Center the view at cursor output
bool View::center()
{
    auto box = get_box();

    auto* seat = input.last_active_seat();
    if (!seat) {
        return false;
    }

    auto* wlr_output = wlr_output_layout_output_at(
        desktop.layout, seat->cursor.wlr_cursor->x, seat->cursor.wlr_cursor->y);
    if (!wlr_output) {
        // empty layout
        return false;
    }

    const wlr::output_layout_output_t* l_output =
        wlr_output_layout_get(desktop.layout, wlr_output);

    int width, height;
    wlr_output_effective_resolution(wlr_output, &width, &height);

    double view_x = (double)(width - box.width) / 2 + l_output->x;
    double view_y = (double)(height - box.height) / 2 + l_output->y;
    move(view_x, view_y);

    return true;
}

ViewChild::~ViewChild() noexcept {}

void ViewChild::finish()
{
    auto keep_alive = util::erase_this(view.children, this);
    view.damage_whole();
}

void ViewChild::handle_commit(void* data)
{
    view.apply_damage();
}

void ViewChild::handle_new_subsurface(void* data)
{
    auto& wlr_subsurface = *(wlr::subsurface_t*)data;
    view.create_subsurface(wlr_subsurface);
}

ViewChild::ViewChild(View& view, wlr::surface_t* wlr_surface)
    : view(view)
    , wlr_surface(wlr_surface)
{
    on_commit = [this](void* data) { handle_commit(data); };
    on_commit.add_to(wlr_surface->events.commit);

    on_new_subsurface = [this] (void* data) { handle_commit(data); };
    on_new_subsurface.add_to(wlr_surface->events.new_subsurface);
}

ViewChild& View::create_child(wlr::surface_t& wlr_surface)
{
    return children.emplace_back(*this, &wlr_surface);
}

Subsurface::Subsurface(View& view, wlr::subsurface_t* wlr_subsurface)
    : ViewChild(view, wlr_subsurface->surface)
    , wlr_subsurface(wlr_subsurface)
{
    on_destroy = [this](void*) { finish(); };
    on_destroy.add_to(wlr_subsurface->events.destroy);
    view.desktop.server.input.update_cursor_focus();
}

Subsurface& View::create_subsurface(wlr::subsurface_t& wlr_subsurface)
{
    cloth_debug("New subsurface");
    return static_cast<Subsurface&>(
        children.push_back(std::make_unique<Subsurface>(*this, &wlr_subsurface)));
}

// Adds a news surface to the view
void View::map(wlr::surface_t& surface)
{
    if (wlr_surface) {
        wlr_surface->data = nullptr;
    }
    wlr_surface = &surface;
    wlr_surface->data = this;

    wlr::subsurface_t* subsurface;
    wl_list_for_each(subsurface, &wlr_surface->subsurfaces, parent_link)
    {
        create_subsurface(*subsurface);
    }

    on_new_subsurface = [this] (void* data) {
        auto& subs = *(wlr::subsurface_t*)data;
        create_subsurface(subs);
    };
    on_new_subsurface.add_to(wlr_surface->events.new_subsurface);

    mapped = true;
    damage_whole();
    desktop.server.input.update_cursor_focus();
}

void View::unmap()
{
    assert(wlr_surface != nullptr);
    wlr_surface->data = nullptr;
    mapped = false;
    remove_seat_focus();
    damage_whole();

    on_new_subsurface.remove();

    for (auto& child : children) {
        child.finish();
    }

    if (fullscreen_output != nullptr) {
        fullscreen_output->context.damage_whole();
        fullscreen_output->workspace->fullscreen_view = nullptr;
        fullscreen_output = nullptr;
    }

    wlr_surface = nullptr;
    width = height = 0;
}

void View::initial_focus()
{
    // TODO what seat gets focus? the one with the last input event?
    workspace->set_focused_view(this);
}

// When the view is spawned it should get focus. 
// Should fill preselect if it is available, else
// it should go fullscreen/maximized if requested.
// and tell the surface which output it is on.
void View::setup()
{
    // TODO: If a new view has been spawned after this one,
    // it should no longer get focus
    initial_focus();

    if (fullscreen_output == nullptr && !maximized) {
        center();
    }

    // Fill the preselection if there is one
    if (workspace->tree.active_preselect()) {
        workspace->tree.fill_preselect(this);
    }
    update_output();
}

void View::save() 
{
        saved.x = lx;
        saved.y = ly;
        saved.width = width;
        saved.height = height;
}

// Damage on surface commit
void View::apply_damage()
{
    for (auto& output : desktop.outputs) {
        output.context.damage_from_view(*this);
    }
}

void View::damage_whole()
{
    for (auto& output : desktop.outputs) {
        output.context.damage_whole_view(*this);
    }
}

// Move view to new position.
void View::update_position(double lx, double ly)
{
    // if at same position do nothing
    if (this->lx == lx && this->ly == ly) {
        return;
    }

    auto before = get_box();

    damage_whole();
    this->lx = lx;
    this->ly = ly;
    damage_whole();

    update_output(before);
}

// Increase or decrease view dimensions
void View::update_size(uint32_t width, uint32_t height)
{
    if (this->width == width && this->height == height) {
        return;
    }

    damage_whole();
    this->width = width;
    this->height = height;
    damage_whole();
}

/**
 * Checks if the view, its subsurfaces or its decorations is at the layout
 * coords. Returns the surface local coordinates of the layout coords if
 * anything is found. Fills wlr_surface with the view's surface or the subsurface that is found.
 */
bool View::at(double lx, double ly, wlr::surface_t*& wlr_surface, double& sx, double& sy)
{
    // If it doesn't have a surface or isn't mapped.
    if (!this->wlr_surface || !this->mapped)
        return false;

    // Something about wl_shell.
    // TODO: Remove, wl_shell isn't supposed to be supported
    if (util::dynamic_is<WlShellSurface>(this) &&
        dynamic_cast<WlShellSurface*>(this)->wl_shell_surface->state ==
            WLR_WL_SHELL_SURFACE_STATE_POPUP) {
        return false;
    }

    // Surface coordinates of lx and ly.
    double view_sx = lx - this->lx;
    double view_sy = ly - this->ly;

    double _sx, _sy;
    wlr::surface_t* _surface = nullptr;
    if (type == ViewType::xdg_shell_v6) {
        _surface = wlr_xdg_surface_v6_surface_at(
            dynamic_cast<XdgSurfaceV6&>(*this).xdg_surface, view_sx, view_sy, &_sx, &_sy);
    } else if (type == ViewType::xdg_shell) {
        _surface = wlr_xdg_surface_surface_at(
            dynamic_cast<XdgSurface&>(*this).xdg_surface, view_sx, view_sy, &_sx, &_sy);
#ifdef WLR_HAS_XWAYLAND
    } else if (type == ViewType::xwayland) {
        _surface = wlr_surface_surface_at(
            dynamic_cast<XwaylandSurface&>(*this).xwayland_surface->surface,
            view_sx,
            view_sy,
            &_sx,
            &_sy);
#endif
    }
    // A surface was found, return it
    if (_surface != nullptr) {
        sx = _sx;
        sy = _sy;
        wlr_surface = _surface;
        return true;
    }

    // A surface was not found, but it was over a part of the ssd
    if (deco.part_at(view_sx, view_sy) != DecoPart::none) {
        sx = view_sx;
        sy = view_sy;
        wlr_surface = nullptr;
        return true;
    }

    return false;
}

} // namespace cloth
