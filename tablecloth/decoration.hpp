#pragma once

#include "wlroots.hpp"

namespace cloth {

    struct View;

    enum struct DecoPart {
        none = 0,
        top_border = (1 << 0),
        bottom_border = (1 << 1),
        left_border = (1 << 2),
        right_border = (1 << 3),
        titlebar = (1 << 4),
    };
    CLOTH_ENABLE_BITMASK_OPS(DecoPart);

    struct Decoration
    {
        Decoration(View& v);

        void update(); 
        void set_visible(bool border = true, bool titlebar = true, bool shadow = true);
        wlr::box_t box() const noexcept; 
        DecoPart part_at(double sx, double sy) const noexcept;
        void damage();
        float shadow_offset();
        float shadow_radius();
        int border_width() 
        { 
            return _border_width; 
        }
        int titlebar_height() 
        { 
            return _titlebar_height; 
        }

        View& view;
        bool visible = false;
        bool border = false;
        bool titlebar = false;
        bool shadow = false;

        private:
        int _border_width = 4;
        int _titlebar_height = 20;
        float _shadow_offset = 0;
        float _shadow_radius = 5;

    };

} // namespace cloth
