#pragma once

#include "wlroots.hpp"

#include "view.hpp"
#include "xcursor.hpp"
#include "gesture.hpp"

namespace cloth {

struct Seat;
struct Tablet;

struct Cursor
{
    enum struct Mode
    {
        Passthrough = 0,
        Move,
        Resize,
    };

    Cursor(Seat& seat, wlr::cursor_t* cursor) noexcept;
    ~Cursor() noexcept;

    // Moves the cursor image, and based on which mode it is in,
    // moves or resizes a view.
    void update_position(uint32_t time);
    void set_image(const char* name);

    // Member data

    Seat& seat;
    Mode mode;

    // Cursor name set when configuring
    std::string default_xcursor = xcursor_default;

    wlr::cursor_t* wlr_cursor = nullptr;
    wlr::xcursor_manager_t* xcursor_manager = nullptr;
    wlr::seat_t* wl_seat = nullptr;
    wl::client_t* last_client = nullptr;

    // Attributes at the start of cursor actions
    DecoPart last_decoration = DecoPart::none;
    int offs_x, offs_y;
    int view_x, view_y, view_width, view_height;
    uint32_t resize_edges;

    wl::Listener on_motion;
    wl::Listener on_motion_absolute;
    wl::Listener on_button;
    wl::Listener on_axis;

    wl::Listener on_touch_down;
    wl::Listener on_touch_up;
    wl::Listener on_touch_motion;

    wl::Listener on_tool_axis;
    wl::Listener on_tool_tip;
    wl::Listener on_tool_proximity;
    wl::Listener on_tool_button;

    wl::Listener on_request_set_cursor;

  private:
    // If the cursor is in passthrough mode this takes care of 
    // negotiating the cursor image with any surface that is beneath it.
    void passthrough_cursor(uint32_t time);
    void press_button(wlr::input_device_t& device,
                      uint32_t time,
                      wlr::Button button,
                      wlr::button_state_t state,
                      double lx,
                      double ly);
    void handle_tablet_tool_position(Tablet& tablet,
                                     wlr::tablet_tool_t* wlr_tool,
                                     bool change_x,
                                     bool change_y,
                                     double x,
                                     double y,
                                     double dx,
                                     double dy,
                                     uint32_t time);
    void deco_cursor(double sx, double sy, View& view);
    void deco_button(double sx,
                     double sy,
                     wlr::Button button,
                     wlr::button_state_t state,
                     View& view);

    std::optional<TouchGesture> current_gesture = std::nullopt;
};

} // namespace cloth

