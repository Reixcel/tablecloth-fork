#include "output.hpp"

#include "wlroots.hpp"

#include "util/logging.hpp"

#include "config.hpp"
#include "layer_shell.hpp"
#include "output.hpp"
#include "seat.hpp"
#include "server.hpp"
#include "view.hpp"

namespace cloth {

render::ViewRenderData get_render_data(View& v)
{
    return { 
        .lx = v.lx,
        .ly = v.ly,
        .width = v.width,
        .height = v.height, 
        .alpha = v.alpha
    };
}

void Output::render()
{
    // Check if the monitor is turned on
    if (!wlr_output.enabled) {
        return;
    }

    context.reset();

    // TODO: Put transition in its own function?
    // If replacing workspace
    if (prev_workspace != workspace && prev_workspace && ws_alpha >= 1.f) {
        ws_alpha = 0;
    }
    // Maybe some kind of fade in
    if (ws_alpha <= 1.f) {
        ws_alpha += 0.1;
    }
    // When finished fading alpha is over 1
    if (ws_alpha > 1.f) {
        ws_alpha = 1.f;
    }

    // Check that the fading is over, then render fullscreen view
    // TODO: Don't know how this looks but maybe it would be cool to just
    // fade directly into the fullscreen_view
    if (prev_workspace == workspace && workspace->fullscreen_view) {
        context.fullscreen_view = workspace->fullscreen_view;
    } else {
        // Fade out old workspace
        float prev_ws_alpha = 1.f - ws_alpha; // 0.925
        if (prev_ws_alpha > 0 && prev_workspace) {
            // Slide?
            double dx = wlr_output.width * ws_alpha;
            if (workspace->index < prev_workspace->index)
                dx = -dx;
            for (auto& v : prev_workspace->visible_views()) {
                auto data = get_render_data(v);
                data.alpha *= prev_ws_alpha;
                data.lx -= dx;
                context.views.emplace_back(v, data);
            }
        }

        // Fade complete
        if (ws_alpha == 1.f) {
            prev_workspace = workspace;
        }

        // Fade in new workspace over old if needed
        if (ws_alpha > 0) {
            double dx = wlr_output.width * (1 - ws_alpha);
            if (prev_workspace && workspace->index < prev_workspace->index) {
                dx = -dx;
            }
            for (auto& v : workspace->visible_views()) {
                auto data = get_render_data(v);
                data.alpha *= ws_alpha;
                data.lx += dx;
                context.views.emplace_back(v, data);
            }
        }
    }

    context.do_render();

    // Fading? damage everything
    if (ws_alpha < 1.f)
        context.damage_whole();
}

static void set_mode(wlr::output_t& output, Config::Output& oc)
{
    int mhz = (int)(oc.mode.refresh_rate * 1000);

    // Output has no mode, try setting a custom one
    if (wl_list_empty(&output.modes)) {
        wlr_output_set_custom_mode(&output, oc.mode.width, oc.mode.height, mhz);
        return;
    }

    wlr::output_mode_t *mode, *best = nullptr;
    wl_list_for_each(mode, &output.modes, link)
    {
        if (mode->width == oc.mode.width && mode->height == oc.mode.height) {
            if (mode->refresh == mhz) {
                best = mode;
                break;
            }
            best = mode;
        }
    }
    if (!best) {
        cloth_error("Configured mode for {} not available", output.name);
    } else {
        cloth_debug("Assigning configured mode to {}", output.name);
        wlr_output_set_mode(&output, best);
    }
}

Output::Output(Desktop& p_desktop, Workspace& ws, wlr::output_t& wlr) noexcept
    : desktop(p_desktop),
      workspace(&ws),
      wlr_output(wlr),
      last_frame(chrono::clock::now())
{
    wlr_output.data = this;
    workspace->output = this;

    cloth_debug("Output '{}' added", wlr_output.name);
    cloth_debug("'{} {} {}' {}mm x {}mm",
         wlr_output.make,
         wlr_output.model,
         wlr_output.serial,
         wlr_output.phys_width,
         wlr_output.phys_height);

    if (!wl_list_empty(&wlr_output.modes)) {
        wlr::output_mode_t* mode = wl_container_of((&wlr_output.modes)->prev, mode, link);
        wlr_output_set_mode(&wlr_output, mode);
    }

    on_destroy.add_to(wlr_output.events.destroy);
    on_destroy = [this] { util::erase_this(desktop.outputs, this); };

    on_mode.add_to(wlr_output.events.mode);
    on_mode = [this] { arrange_layers(*this); };

    on_transform.add_to(wlr_output.events.transform);
    on_transform = [this] { arrange_layers(*this); };

    on_damage_frame.add_to(context.damage->events.frame);
    on_damage_frame = [this] { render(); };

    on_damage_destroy.add_to(context.damage->events.destroy);
    on_damage_destroy = [this] { util::erase_this(desktop.outputs, this); };

    Config::Output* output_config = desktop.config.get_output(wlr_output);
    if (output_config) {
        if (output_config->enable) {
            if (wlr_output_is_drm(&wlr_output)) {
                for (auto& mode : output_config->modes) {
                    wlr_drm_connector_add_mode(&wlr_output, &mode.info);
                }
            } else {
                if (output_config->modes.empty()) {
                    cloth_error("Can only add modes for DRM backend");
                }
            }
            if (output_config->mode.width) {
                set_mode(wlr_output, *output_config);
            }
            wlr_output_set_scale(&wlr_output, output_config->scale);
            wlr_output_set_transform(&wlr_output, output_config->transform);
            wlr_output_layout_add(
                desktop.layout, &wlr_output, output_config->x, output_config->y);
        } else {
            wlr_output_enable(&wlr_output, false);
        }
    } else {
        wlr_output_layout_add_auto(desktop.layout, &wlr_output);
    }

    arrange_layers(*this);
    context.damage_whole();
}

} // namespace cloth
