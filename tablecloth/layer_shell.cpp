#include "layer_shell.hpp"

#include "util/algorithm.hpp"
#include "util/exception.hpp"
#include "util/logging.hpp"
#include "wlroots.hpp"

#include "desktop.hpp"
#include "output.hpp"
#include "seat.hpp"
#include "server.hpp"

namespace cloth {

using namespace std::literals;

LayerPopup::LayerPopup(LayerSurface& p_parent, wlr::xdg_popup_v6_t& p_wlr_popup)
    : parent(p_parent)
    , wlr_popup(p_wlr_popup)
{
    // Remove popup
    on_destroy = [this] { util::erase_this(parent.children, this); };
    on_destroy.add_to(wlr_popup.base->events.destroy);

    // popup creates new popup
    on_new_popup = [this](void* data) {
        parent.create_popup(*((wlr::xdg_popup_v6_t*)data));
    };
    on_new_popup.add_to(wlr_popup.base->events.new_popup);

    // Coords should be given in specific context
    // Damage popup at layout coords?
    auto damage_whole = [this] {
        int ox = wlr_popup.geometry.x + parent.geo.x;
        int oy = wlr_popup.geometry.y + parent.geo.y;
        parent.output.context.damage_whole_local_surface(*wlr_popup.base->surface, ox, oy);
    };

    // Render popup, check for focus
    on_map.add_to(wlr_popup.base->events.map);
    on_map = [this, damage_whole] {
        damage_whole();
        parent.output.desktop.server.input.update_cursor_focus();
    };

    // Render Popup, don't check for focus?
    on_unmap.add_to(wlr_popup.base->events.unmap);
    on_unmap = damage_whole;

    // Render Popup
    on_commit.add_to(wlr_popup.base->surface->events.commit);
    on_commit = damage_whole;

    // TODO: Desired behaviour?
}

// Exclusive area for box
static void apply_exclusive(wlr::box_t& usable_area,
                            uint32_t anchor,
                            int32_t exclusive,
                            // margins
                            int32_t margin_top,
                            int32_t margin_right,
                            int32_t margin_bottom,
                            int32_t margin_left)
{
    if (exclusive <= 0) {
        return;
    }
    struct
    {
        uint32_t anchors;
        int* positive_axis;
        int* negative_axis;
        int margin;
    } edges[] = {
        {
            // top bar
            .anchors = ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP,
            .positive_axis = &usable_area.y,
            .negative_axis = &usable_area.height,
            .margin = margin_top,
        },
        {
            // bottom bar
            .anchors = ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = nullptr,
            .negative_axis = &usable_area.height,
            .margin = margin_bottom,
        },
        {
            // left bar
            .anchors = ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = &usable_area.x,
            .negative_axis = &usable_area.width,
            .margin = margin_left,
        },
        {
            // right bar
            .anchors = ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                       ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = nullptr,
            .negative_axis = &usable_area.width,
            .margin = margin_right,
        },
    };

    // Check that the provided anchor is the same as the edge
    // Add exclusive + margin to get the size of the exclusive zone
    for (size_t i = 0; i < sizeof(edges) / sizeof(edges[0]); ++i) {
        if ((anchor & edges[i].anchors) == edges[i].anchors) {
            if (edges[i].positive_axis) {
                *edges[i].positive_axis += exclusive + edges[i].margin;
            }
            if (edges[i].negative_axis) {
                *edges[i].negative_axis -= exclusive + edges[i].margin;
            }
        }
    }
}

// update the usable area to not intersect with exclusive areas
static void arrange_layer(wlr::output_t& output,
                          util::ptr_vec<LayerSurface>& list,
                          wlr::box_t& usable_area,
                          bool exclusive)
{
    wlr::box_t full_area = { 0 };
    wlr_output_effective_resolution(&output, &full_area.width, &full_area.height);
    for (auto& surface : list) {
        wlr::layer_surface_v1_t& layer = surface.layer_surface;
        wlr::layer_surface_v1_state_t& state = layer.current;

        // surface doesn't have an exclusive_zone
        if (exclusive != (state.exclusive_zone > 0)) {
            continue;
        }

        wlr::box_t bounds;
        if (state.exclusive_zone == -1) {
            bounds = full_area;
        } else {
            bounds = usable_area;
        }
        wlr::box_t box = { .width = (int)state.desired_width,
                           .height = (int)state.desired_height };
        // Horizontal axis
        const uint32_t both_horiz =
            ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT;
        if ((state.anchor & both_horiz) && box.width == 0) {
            // TODO: Why would you need to check on both, == both_horiz implies
            // box.width = 0 no desired length, spa entire width
            box.x = bounds.x;
            box.width = bounds.width;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT)) {
            // Left side of bounds
            box.x = bounds.x;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT)) {
            // Right side of bounds
            box.x = bounds.x + (bounds.width - box.width);
        } else {
            // center of bounds
            box.x = bounds.x + ((bounds.width / 2) - (box.width / 2));
        }
        // Vertical axis
        const uint32_t both_vert =
            ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP | ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM;
        if ((state.anchor & both_vert) && box.height == 0) {
            // No desired height, span entire height
            box.y = bounds.y;
            box.height = bounds.height;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP)) {
            // Top side of bounds
            box.y = bounds.y;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM)) {
            // Bottom side of bounds
            box.y = bounds.y + (bounds.height - box.height);
        } else {
            // centered vertically in bounds
            box.y = bounds.y + ((bounds.height / 2) - (box.height / 2));
        }
        // Margin
        if ((state.anchor & both_horiz) == both_horiz) {
            box.x += state.margin.left;
            box.width -= state.margin.left + state.margin.right;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT)) {
            box.x += state.margin.left;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT)) {
            box.x -= state.margin.right;
        }
        if ((state.anchor & both_vert) == both_vert) {
            box.y += state.margin.top;
            box.height -= state.margin.top + state.margin.bottom;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP)) {
            box.y += state.margin.top;
        } else if ((state.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM)) {
            box.y -= state.margin.bottom;
        }
        if (box.width < 0 || box.height < 0) {
            // TODO: Bubble up a protocol error?
            wlr_layer_surface_v1_close(&layer);
            continue;
        }
        surface.geo = box;
        // Tell the surface to assume the dimenstions
        wlr_layer_surface_v1_configure(&layer, box.width, box.height);
    }
}

void arrange_layers(Output& output)
{
    wlr::box_t usable_area = { 0 };
    wlr_output_effective_resolution(
        &output.wlr_output, &usable_area.width, &usable_area.height);

    // Arrange exclusive surfaces from top->bottom
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY],
                  usable_area,
                  true);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP],
                  usable_area,
                  true);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM],
                  usable_area,
                  true);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND],
                  usable_area,
                  true);
    memcpy(&output.usable_area, &usable_area, sizeof(wlr::box_t));

    // Rearrange maximized view to take up less space.
    for (View& view : output.workspace->visible_views()) {
        if (view.maximized) {
            view.maximize(true);
        }
    }

    // Arrange non-exlusive surfaces from top->bottom
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY],
                  usable_area,
                  false);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP],
                  usable_area,
                  false);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM],
                  usable_area,
                  false);
    arrange_layer(output.wlr_output,
                  output.layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND],
                  usable_area,
                  false);

    // Find topmost keyboard interactive layer, if such a layer exists
    uint32_t layers_above_shell[] = {
        ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
        ZWLR_LAYER_SHELL_V1_LAYER_TOP,
    };
    size_t nlayers = sizeof(layers_above_shell) / sizeof(layers_above_shell[0]);
    LayerSurface* topmost = nullptr;
    for (size_t i = 0; i < nlayers; ++i) {
        for (auto& layer : util::view::reverse(output.layers[layers_above_shell[i]])) {
            if (layer.layer_surface.current.keyboard_interactive) {
                topmost = &layer;
                break;
            }
        }
        if (topmost != nullptr) {
            break;
        }
    }

    for (auto& seat : output.desktop.server.input.seats) {
        seat.set_focus_layer(topmost ? &topmost->layer_surface : nullptr);
    }
}

// If the new layer_shell surface doesn't have an output, assign it one else exit.
// Tries to assign to the output of the pointer first, if not found use
// the center monitor.
void Desktop::handle_layer_shell_surface(void* data)
{
    auto& layer_surface = *(wlr::layer_surface_v1_t*)data;

    cloth_debug("new layer surface: namespace {} layer {} anchor {} size {}x{} margin "
         "{},{},{},{}",
         layer_surface.namespace_,
         layer_surface.layer,
         layer_surface.layer,
         layer_surface.client_pending.desired_width,
         layer_surface.client_pending.desired_height,
         layer_surface.client_pending.margin.top,
         layer_surface.client_pending.margin.right,
         layer_surface.client_pending.margin.bottom,
         layer_surface.client_pending.margin.left);

    if (!layer_surface.output) {
        Seat* seat = server.input.last_active_seat();
        assert(seat); // TODO: Technically speaking we should handle this case
        wlr::output_t* output = wlr_output_layout_output_at(
            layout, seat->cursor.wlr_cursor->x, seat->cursor.wlr_cursor->y);
        if (!output) {
            cloth_error("Couldn't find output at (%.0f,%.0f)",
                 seat->cursor.wlr_cursor->x,
                 seat->cursor.wlr_cursor->y);
            output = wlr_output_layout_get_center_output(layout);
        }
        if (output) {
            layer_surface.output = output;
        } else {
            wlr_layer_surface_v1_close(&layer_surface);
            return;
        }
    }

    auto* output = output_from_wlr_output(layer_surface.output);
    if (!output)
        output = &outputs.front();

    [[maybe_unused]] auto& layer =
        output->layers[layer_surface.layer].emplace_back(*output, layer_surface);

    // Temporarily set the layer's current state to client_pending
    // So that we can easily arrange it
    wlr::layer_surface_v1_state_t old_state = layer_surface.current;
    layer_surface.current = layer_surface.client_pending;

    arrange_layers(*output);

    layer_surface.current = old_state;
}

LayerPopup& LayerSurface::create_popup(wlr::xdg_popup_v6_t& wlr_popup)
{
    auto popup = std::make_unique<LayerPopup>(*this, wlr_popup);
    return children.push_back(std::move(popup));
}

LayerSurface::LayerSurface(Output& p_output, wlr::layer_surface_v1_t& p_layer_surface)
    : output(p_output)
    , layer_surface(p_layer_surface)
{
    layer_surface.data = this;

    if (p_layer_surface.namespace_ == "cloth.notificaion"sv) {
        has_shadow = true;
    }

    // New surface frame
    on_surface_commit.add_to(layer_surface.surface->events.commit);
    on_surface_commit = [this](void* data) {
        wlr::box_t old_geo = geo;
        arrange_layers(output);
        // damage box in old and new position
        if (old_geo != geo) {
            output.context.damage_whole_layer(*this, old_geo);
        }
        output.context.damage_whole_layer(*this);
    };

    // Output gone, remove surface
    on_output_destroy.add_to(layer_surface.output->events.destroy);
    on_output_destroy = [this](void* data) {
        layer_surface.output = nullptr;
        on_output_destroy.remove();
        wlr_layer_surface_v1_close(&layer_surface);
    };

    // Destroy surface
    on_destroy.add_to(layer_surface.events.destroy);
    on_destroy = [this](void* data) {
        if (layer_surface.mapped) {
            output.context.damage_whole_layer(*this);
        }
        auto keep_alive = util::erase_this(output.layers[layer_surface.layer], this);
        arrange_layers(output);
    };

    // Render surface, tell surface which output it's in
    on_map.add_to(layer_surface.events.map);
    on_map = [this](void* data) {
        output.context.damage_whole_layer(*this);
        wlr_surface_send_enter(layer_surface.surface, &output.wlr_output);
    };

    // Remove render
    // If it had focus cursor needs to update focus
    on_unmap.add_to(layer_surface.events.unmap);
    on_unmap = [this](void* data) {
        output.context.damage_whole_layer(*this);
        output.desktop.server.input.update_cursor_focus();
    };

    // Add a popup
    on_new_popup.add_to(layer_surface.events.new_popup);
    on_new_popup = [this](void* data) {
        auto& wlr_popup = *(wlr::xdg_popup_v6_t*)data;
        create_popup(wlr_popup);
    };
    // TODO: Listen for subsurfaces
}

} // namespace cloth

// kak: other_file=layers.hpp
