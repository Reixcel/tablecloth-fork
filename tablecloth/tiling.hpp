#pragma once

#include <vector>

#include "util/ptr_vec.hpp"

#include "wlroots.hpp"
#include "view.hpp"


/*
 *          a     
 *         / \
 *        1   b   
 *           / \
 *          2   3
 *
 * All nodes above are leaves. The parent leaves(represented by letters) 
 * do not contain views; their only function is to represent the structure of other leaves.
 * A parent is allowed to have an arbitrary amount of child views.
 */

namespace cloth {

struct Workspace;

enum struct Split { none, horiz, vert };

enum struct Direction { up, down, left, right };

struct Leaf 
{
    Leaf(Leaf* parent) : parent(parent) {};

    auto add_child(util::shared_ptr_vec<Leaf>::iterator it) -> std::shared_ptr<Leaf>&; 
    // Remove itself from its parent's children
    auto remove() -> std::shared_ptr<Leaf>; 
    // Iterator in its parents "children" vector
    auto location() -> util::shared_ptr_vec<Leaf>::iterator;
    bool is_child();

    Leaf* parent; // nullptr when root
    wlr::box_t area; // Updated on arrange

    // Data when parent
    util::shared_ptr_vec<Leaf> children;
    Split split = Split::none;

    // Data when child
    View* view = nullptr; // This is probably not skookum but what can you do
    float split_ratio = 1; // Changed when creating new leaf unless root leaf
};

enum struct Layout { tiling, monocle, fullscreen };

struct TileTree 
{
    TileTree(Workspace& ws);

    // Preselection and view handling
    Leaf* sibling(Leaf*& first, Direction dir);
    Leaf* leaf_at(double lx, double ly);
    auto leafptr_from_view(View* view) -> std::shared_ptr<Leaf>;
    int depth(Leaf* leaf);
    void closest_siblings(Leaf* first, Leaf* second);
    void toggle_tile(View* view);
    void mark_preselect(Direction dir);
    bool fill_preselect(View* view); 
    bool active_preselect();
    void move_to_preselect(Leaf& leaf);
    void remove_selection(); // Remember to update split_dir if only one child
    auto remove_leaf(Leaf& child) -> std::shared_ptr<Leaf>;
    void set_focus(View* view);

    // Tile size and position functions
    wlr::box_t usable_area();
    void arrange_root();
    void arrange(Leaf& leaf, wlr::box_t area);
    float check_resize(Leaf& leaf, Split split, float percent);
    void resize_tile(Leaf& leaf, Direction dir, float percent); 
    void resize_tile(Leaf& leaf, Direction dir, int pixels); 
    void resize_tile(Leaf& leaf, double lx, double ly, uint32_t edges);
    void normalize(Leaf& parent);

    // Functions called by the user
    void move_in_dir(Leaf& leaf, Direction dir); // Remember to use swap when implmeenting
    void swap_tiles(Leaf& first, Leaf& second);

    // All views in workspace that are tiled
    util::ref_vec<View> tiled_views();

    Workspace& workspace; 
    Layout layout = Layout::tiling;


  private:
    bool add_leaf(Leaf& parent,
                  Leaf& amputee,
                  int location,
                  View* view);
    void resize_leaves(Leaf& brother, Leaf& sister, float abs_percent);
    Leaf* leaf_in_dir(Leaf& leaf, Direction dir);
    Leaf& largest_leaf();
    
    /**
     * '_root' has special behaviour.
     * If the tree is emtpy, it always contains a preselect, but it is not drawn.
     */
    Leaf _root = Leaf(nullptr); 
    util::shared_ptr_vec<Leaf> _leaves; // non sorted list of all children
    std::shared_ptr<Leaf> _preselect; 
    std::shared_ptr<Leaf> _special_leaf = nullptr; // Maximized/fullscreen
};

} // namepspace cloth
