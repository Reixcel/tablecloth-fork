#include "workspace.hpp"

#include "util/chrono.hpp"
#include "util/ptr_vec.hpp"

#include "desktop.hpp"
#include "layer_shell.hpp"
#include "output.hpp"
#include "seat.hpp"
#include "server.hpp"
#include "view.hpp"
#include "wlroots.hpp"

#include "util/iterators.hpp"
#include "util/logging.hpp"

namespace cloth {

// Return a reference vector of all mapepd views.
util::ref_vec<View> Workspace::visible_views()
{
    return util::view::filter(_views, [](auto& v) { return v.mapped; });
}

// Return the latest view to have focus.
View* Workspace::focused_view()
{
    auto visviews = visible_views();
    return visviews.begin() == visviews.end() ? nullptr : &visviews.back();
}

// Return the latest tiled view to have focus.
View* Workspace::last_focused_tile_view()
{
    auto it = std::find_if(
            _views.rbegin(),
            _views.rend(),
            [](auto& view) { return view.state == ViewState::tiled; });

    if (it != _views.rend()) {
        return &*it;
    }
    return nullptr;
}

Leaf* Workspace::last_focused_tile_leaf()
{
    auto view = last_focused_tile_view();

    if (view) {
        auto leaf = tree.leafptr_from_view(view);
        if (!leaf) {
            cloth_error(
                    "A view was reported as tiled, \
                    but the corresponding tile could not be found");
            return nullptr;
        } else {
            return leaf.get();
        }
    }
    return nullptr;
}

// Check if this workspace is the focused workspace for the desktop
bool Workspace::is_current()
{
    return &desktop.current_workspace() == this;
}

// Check if this workspace is visible on any monitors
bool Workspace::is_visible()
{
    return util::any_of(desktop.outputs,
                        [this](Output& output) { return output.workspace == this; });
}


View* Workspace::set_focused_view(View* view)
{
    if (view == nullptr) {
        return nullptr;
    }
    cloth_debug("Set focus: name='{}'", view->get_name());
    View* prev_focus = focused_view();

    _views.rotate_to_back(*view);

    if (is_current()) {
        for (auto&& seat : desktop.server.input.seats) {
            seat.set_focus(view);
        };
    }

    bool unfullscreen = true;

#ifdef WLR_HAS_XWAYLAND
    if (auto* xwl_view = dynamic_cast<XwaylandSurface*>(view);
        xwl_view && xwl_view->xwayland_surface->override_redirect) {
        unfullscreen = false;
    }
#endif

    if (view && unfullscreen) {
        wlr::box_t box = view->get_box();
        if (this->fullscreen_view
            && this->fullscreen_view != view 
            && wlr_output_layout_intersects(desktop.layout, &output->wlr_output, &box)
            ) {
            this->fullscreen_view->set_fullscreen(false, nullptr);
        }
    }

    desktop.server.window_manager.send_focused_window_name(*this);

    if (view == prev_focus) {
        return view;
    }

#ifdef WLR_HAS_XWAYLAND
    if (auto* xwl_view = dynamic_cast<XwaylandSurface*>(view);
        xwl_view && !wlr_xwayland_or_surface_wants_focus(xwl_view->xwayland_surface)) {
        return view;
    }
#endif

    return view;
}

View* Workspace::cycle_focus()
{
    auto views = visible_views();
    auto rviews = util::view::reverse(views);
    if (views.empty()) {
        return nullptr;
    }

    auto first_view = &*rviews.begin();
    if (!first_view->is_focused()) {
        return set_focused_view(first_view);
    }
    auto next_view = ++rviews.begin();
    if (next_view != rviews.end()) {
        // Focus the next view. Invalidates the iterator next_view;
        auto nvp = set_focused_view(&*next_view);
        // Move the first view to the front of the list
        _views.rotate_to_front(*first_view);
        return nvp;
    }
    return nullptr;
}

View& Workspace::add_view(std::unique_ptr<View>&& view_ptr)
{
    view_ptr->workspace = this;
    view_ptr->damage_whole();
    return _views.push_back(std::move(view_ptr));
}

std::unique_ptr<View> Workspace::erase_view(View& v)
{
    v.damage_whole();
    return _views.erase(v);
}

} // namespace cloth
